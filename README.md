# Flask Starter Kit

### Comes with preconfigured basic Flask app with SQLAlchmemy, LoginForm, Basic User Model, and static assets.

### Requirements:
* Python 2.7.x
* `node` with global installation of `gulp` and `bower`. Can be used without that global installations also.

### How to use ?
* Clone this repo `git clone git@github.com:brijeshb42/flask-web-starter-kit.git`.
* Create a new virtual environment `mkvirtualenv starter-kit`.
* Switch to the new env `workon starter-kit`.
* Install python modules `pip install -r requirements.txt`.
* Install node modules to compile assets `npm install`.
* Install bower packages `bower install`.
* Copy `config.py.default` to `config.py`.
* Make changes to `config.py` as required.
* Create a symlink to `bower_components` folder inside `frontend` : `cd frontend && ln -s ../bower_components/ bower_components`.
* Migrate DB (uses sqlite by default. Can be configured in `config.py`)
    * `python script.py db init`
    * `python script.py db migrate`
    * `python script.py db upgrade`
* In separate terminal, `cd` into the `starter-kit` directory and run `gulp clean && gulp` to compile static assets and start a livereload server.
* Then run `python script.py runserver`
* Open `localhost:5000` in browser.
* To add your own routes, open `backend/app.py` and start adding your own urls and responses. You can spin off your routes in new files. Just don't forget to import your files in `app.py`.

### How to restore AWS Dynamodb
Please configure virutalenv and install requirements.txt first.

Then, use dynamodump.py script to restore the database.
```
python dynamodump.py -m restore -p default -r eu-west-3 --dumpPath ./dump -s [source table] -d [destination table]
```


### How to run docker container on server
```
docker run -d -p 81:80 --name qcontainer -v$(pwd):/app songxunzhao/questionnaire:1.2
```
To see logs, run the following command
```
docker logs -f [container id]
``` 
