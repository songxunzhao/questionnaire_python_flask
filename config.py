import os
from dotenv import load_dotenv

# from huey import RedisHuey
basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(basedir + '/.env', verbose=True)
# queue = RedisHuey("indexer", host="localhost", port=6379)


class Config(object):
    APP_NAME = 'Questionnaire'
    APP_DESC = 'Online Survey Application'
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'somethingsecret'
    REMEMBER_COOKIE_NAME = 'starter-kit-app'
    CACHE_KEY_PREFIX = 'starter'
    CACHE_DEFAULT_TIMEOUT = 1800
    REGISTER_PATH = os.path.join(basedir, os.environ['REGISTER_PATH'])
    EXPORT_PATH = os.path.join(basedir, os.environ['EXPORT_PATH'])
    UPLOAD_PATH = os.path.join(basedir, os.environ['UPLOAD_PATH'])
    UPLOAD_URL = os.path.join(basedir, os.environ['UPLOAD_URL'])
    REGISTER_URL = os.environ['REGISTER_URL']
    SUPPORTED_LANGUAGES = {'bg': 'Bulgarian', 'en': 'English', 'fr': 'Francais'}
    BABEL_DEFAULT_LOCALE = 'fr'
    BABEL_DEFAULT_TIMEZONE = 'UTC'

    @staticmethod
    def init_app(app):
        pass

    def __init__(self):
        pass


class DevConfig(Config):
    TESTING = True

    def __init__(self):
        super(DevConfig, self).__init__()


class TestConfig(Config):
    TESTING = True

    def __init__(self):
        super(TestConfig, self).__init__()


class ProdConfig(Config):
    TESTING = False

    def __init__(self):
        super(ProdConfig, self).__init__()


config = {
    'development': DevConfig,
    'test': TestConfig,
    'production': ProdConfig,
    'default': DevConfig
}


class Settings:
    MAIL_HOST = 'smtp.mandrillapp.com'
    MAIL_PORT = 587
    MAIL_TO = ['user@gmail.com']
    MAIL_FROM = 'admin@gmail.com'
    MAIL_USERNAME = 'auth@gmail.com'
    MAIL_PASSWORD = 'MailPassword-JxxQ'
