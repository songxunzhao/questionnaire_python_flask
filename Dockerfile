FROM tiangolo/meinheld-gunicorn-flask:python2.7

RUN apt update -y
RUN apt install npm -y
RUN npm install -g gulp-cli
RUN npm install -g bower

COPY . /app/
WORKDIR /app/

RUN pip install -r requirements.txt
RUN npm install
RUN bower install --allow-root
RUN gulp compile
RUN ln -s ./bower_components/ ./frontend/bower_components
ENV FLASK_ENV production




