var fs   = require('fs');
var gulp        = require("gulp"),
    minifyCss   = require("gulp-minify-css"),
    uglify      = require("gulp-uglify"),
    minify      = require("gulp-minify"),
    rev         = require("gulp-rev"),
    del         = require("del"),
    plumber     = require("gulp-plumber"),
    usemin      = require("gulp-usemin"),
    htmlmin     = require("gulp-htmlmin"),
    less        = require("gulp-less"),
    jshint      = require('gulp-jshint'),
    livereload  = require('gulp-livereload');

var src = {
    root: "./frontend/",
    html: "./frontend/**//*.html",
    js: "./frontend/static/js/",
    css: "./frontend/static/css/",
    less: "./frontend/static/less/",
    img: "./frontend/static/img/",
    fonts: "./frontend/static/fonts/",
    register: "./frontend/static/register/"
};

var dest = {
    root: "./templates/",
    html: "./templates/",
    css: "./templates/static/css/",
    js: "./templates/static/js/",
    img: "./templates/static/img/",
    fonts: "./templates/static/fonts/",
    register: "./templates/static/register/",
    exportDir: "./templates/static/export/",
    uploadDir: "./templates/static/upload/"
};

gulp.task("clean", function() {
    del([dest.root, src.css], function (err, paths) {
        console.log('Deleted files/folders:\n', paths.join('\n'));
    });
});
gulp.task("create-dirs", function (done) {
    if(!fs.existsSync(dest.exportDir)) {
        fs.mkdirSync(dest.exportDir);
        console.log('📁  folder created:', dest.exportDir);
    }

    if(!fs.existsSync(dest.uploadDir)) {
        fs.mkdirSync(dest.uploadDir);
        console.log('📁  folder created:', dest.uploadDir);
    }

    done();
});

gulp.task("copy-img", function() {
    return gulp.src(src.img+"**/*")
        .pipe(gulp.dest(dest.img));
});

gulp.task("copy-fonts", function() {
    return gulp.src(src.fonts+"**/*")
        .pipe(gulp.dest(dest.fonts));
});

gulp.task("copy-register", function() {
    return gulp.src(src.register+"**/*")
        .pipe(gulp.dest(dest.register));
});

gulp.task("js", function() {
    return gulp.src(src.js+"*.js")
        .pipe(jshint())
        .pipe(jshint.reporter("default"))
        .pipe(gulp.dest(dest.js));
});

gulp.task("js-prod", function() {
    return gulp.src(src.js+"*.js")
        .pipe(uglify())
        .pipe(gulp.dest(dest.js));
});

gulp.task("less", function() {
    return gulp.src(src.less+"style.less")
        .pipe(plumber())
        .pipe(less())
        .pipe(gulp.dest(src.css))
        .pipe(gulp.dest(dest.css))
        .pipe(livereload());
});

gulp.task("less-prod", function() {
    return gulp.src(src.less+"style.less")
        .pipe(plumber())
        .pipe(less())
        .pipe(minifyCss({keepSpecialComments: 0}))
        .pipe(gulp.dest(src.css));
});

gulp.task("usemin", gulp.series("less", function() {
    return gulp.src([src.html])
        .pipe(plumber())
        .pipe(usemin({
            css: [],
            html: [],
            js: []
        }))
        .pipe(gulp.dest(dest.html))
        .pipe(livereload());
}));

gulp.task("usemin-prod", gulp.series("less-prod", function() {
    return gulp.src(src.html)
        .pipe(plumber())
        .pipe(usemin({
            css: [],
            html: [],
            js: [function() {return uglify({mangle: false})}]
        }))
        .pipe(gulp.dest(dest.html));
}));

gulp.task("htmlmin", gulp.series("usemin-prod", function() {
    return gulp.src(dest.html + "**//*.html")
        .pipe(htmlmin({
            removeComments: true,
            removeCommentsFromCDATA: true
        }))
        .pipe(gulp.dest(dest.html));
}));

gulp.task("cssmin", gulp.series("htmlmin", function() {
    return gulp.src(dest.css+"*.css")
        .pipe(minifyCss({keepSpecialComments: 0}))
        .pipe(gulp.dest(dest.css));
}));

gulp.task("watch", gulp.series(
  "copy-img",
  "copy-fonts",
  "copy-register",
  "usemin",
  "create-dirs",
  function() {
    livereload.listen({
        port: 35729,
        start: true
    });
    console.info('Livereload on PORT '+livereload.options.port);
    gulp.watch(src.html, gulp.series("usemin"));
    gulp.watch(src.less+"*.less", gulp.series("less"))
    gulp.watch(src.js+"*.js", gulp.series("js"))
}));

gulp.task("compile", gulp.series(
  "copy-img",
  "copy-fonts",
  "copy-register",
  "create-dirs",
  "usemin-prod",
  function (done) { done()})
);
gulp.task("default", gulp.series("watch", function (done) { done()}));

