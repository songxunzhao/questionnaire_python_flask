from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, jsonify, abort
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )

from backend.data_sensibility.models import DataSensibility

bp = Blueprint('api.data_sensibility', __name__)


@bp.route('/all', methods=['GET'])
@login_required
def get_all():
    sectors = DataSensibility.scan()
    sector_list = []
    for sector in sectors:
        sector_list.append(sector.name)
    return jsonify(sector_list)


@bp.route('/add', methods=['POST'])
@login_required
def add():
    print request.form
    sector_name = request.form.get('name')
    if not sector_name:
        return jsonify({'error': 'Bad Request'}), 404

    sector = DataSensibility()
    sector.name = sector_name
    sector.description = request.args.get('description', '')
    sector.save()

    return jsonify(sector.to_dict())
