from flask import Flask, render_template, request, g, abort, redirect, url_for
from flask_babel import Babel
from flask_login import login_required, current_user
from werkzeug.contrib.fixers import ProxyFix
from flask_caching import Cache
from flask_bootstrap import Bootstrap

from backend.companies.models import Company
from .filters import register_filters
import i18n
import logging
import sys

import os

from backend import contractors, assignments, tasks, settings, activity_sectors, treatment_services, data_sensibility, categories
from . import auth
from . import users
from . import companies
from config import config
from datetime import datetime

from assignments.models import Treatment
from tasks.models import Task
from contractors.models import Contractor
from backend.assignments.choice import TreatmentStatusTypes
from backend.tasks.choice import TaskStatusTypes

__version__ = "0.1.0"
__author__ = "Brijesh Bittu <brijeshb42@gmail.com>"

"""App setup."""

flask_env = os.environ.get('FLASK_ENV')
app = Flask(
    __name__,
    template_folder="../templates/",
    static_url_path="/static",
    static_folder="../templates/static/")
Bootstrap(app)
app.wsgi_app = ProxyFix(app.wsgi_app)
app.config.from_object(config[flask_env])

i18n.init(app)

cache = Cache(config={"CACHE_TYPE": "simple"})
cache.init_app(app)
logger = logging.Logger(config[flask_env].APP_NAME)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

register_filters(app)

auth.init_app(app)
users.init_app(app)
contractors.init_app(app)
companies.init_app(app)
assignments.init_app(app)
tasks.init_app(app)
settings.init_app(app)
activity_sectors.init_app(app)
treatment_services.init_app(app)
data_sensibility.init_app(app)
categories.init_app(app)

# Home page
@app.route("/")
@login_required
def lang_index():
    lang = app.config['BABEL_DEFAULT_LOCALE']
    if current_user.is_authenticated:
        company_id = current_user.company_id
        try:
            company = Company.get(company_id)
            lang = company.language
        except Company.DoesNotExist:
            pass

    return redirect(url_for('index', lang_code=lang))


@app.route("/<lang_code>", methods=['GET'])
@login_required
def index():
    username = current_user.username

    # get filter arguments
    fromDate = request.args.get('from')
    toDate = request.args.get('to')

    fromDateVal = None
    toDateVal = None

    if fromDate:
        fromDateVar = datetime.strptime(fromDate, '%m/%d/%Y').date()
        fromDateVal = datetime(fromDateVar.year, fromDateVar.month, fromDateVar.day)
    if toDate:
        toDateVar = datetime.strptime(toDate, '%m/%d/%Y').date()
        toDateVal = datetime(toDateVar.year, toDateVar.month, toDateVar.day)

    TOP_RECORD_COUNTS = 5

    # get record counts
    treatments = Treatment.scan()

    treatment_count = 0
    validated_count = 0
    pia_count = 0
    top_treatments = []

    treatment_count_per_type = {}

    for treatment in treatments:
        if not current_user.is_admin and current_user.company_id != treatment.company_id:
            continue

        created_time = treatment.created_at.replace(tzinfo=None)
        if (fromDateVal and created_time < fromDateVal) or (toDateVal and created_time > toDateVal):
            continue
        treatment_count += 1

        status = treatment.status
        if treatment_count_per_type.__contains__(status):
            treatment_count_per_type[status] += 1
        else:
            treatment_count_per_type[status] = 1

        if treatment.status == TreatmentStatusTypes.VALIDATED.name:
          validated_count += 1
        if treatment.need_PIA:
            pia_count += 1
        if treatment_count <= 5:
            top_treatments.append(treatment)

    tasks = Task.scan()

    task_count = 0
    top_tasks = []
    task_count_per_type = {}

    for task in tasks:
        if not current_user.is_admin and current_user.company_id != task.company_id:
            continue

        updated_time = task.updated_at.replace(tzinfo=None)
        if (fromDateVal and updated_time < fromDateVal) or (toDateVal and updated_time > toDateVal):
            continue
        task_count += 1

        status = task.status
        if task_count_per_type.__contains__(status):
            task_count_per_type[status] += 1
        else:
            task_count_per_type[status] = 1

        if task_count <= 5:
            top_tasks.append(task)

    contractors = Contractor.scan()

    contractor_count = 0
    for contractor in contractors:
        if not current_user.is_admin and current_user.company_id != contractor.company_id:
            continue

        contractor_count += 1

    colors = [
        "#F7464A", "#46BFBD", "#FDB45C", "#FEDCBA",
        "#ABCDEF", "#DDDDDD", "#ABCABC", "#4169E1",
        "#C71585", "#FF4500", "#FEDCBA", "#46BFBD"]

    return render_template(
        "index.html", username=username, treatment_count=treatment_count,
        task_count=task_count, contractor_count=contractor_count, validated_count=validated_count, pia_count=pia_count,
        top_n_treatments=top_treatments, top_n_tasks=top_tasks,
        treatment_labels=list(treatment_count_per_type),
        treatment_values=list(treatment_count_per_type.values()),
        colors=colors,
        task_labels=list(task_count_per_type),
        task_values=list(task_count_per_type.values()),
        fromDate=fromDate,
        toDate=toDate,
        treatment_status_types=dict(TreatmentStatusTypes.choices()),
        task_status_types=dict(TaskStatusTypes.choices())
    )

@app.route("/403")
@login_required
def error_403():
    return render_template("403.html")


@app.route("/404")
@login_required
def error_404():
    return render_template("404.html")


if __name__ == "__main__":
    app.run()
