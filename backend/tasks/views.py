import datetime
import os
import json
import unicodecsv as csv
import io

from flask import (
    render_template,
    flash,
    url_for,
    redirect,
    request,
    Response,
    Blueprint,
    abort,
    current_app as app,
    g
)
from flask_babel import gettext

from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )
from werkzeug.utils import secure_filename

from backend.assignments.models import Treatment
from backend.auth.models import AuthUser
from backend.decorators import is_company_user, is_dpo
from backend.models import IDGenerator
from backend.tasks.choice import priority_choice, CategoryChoice, TaskStatusTypes, task_difficulty_choice
from backend.tasks.forms import ManualTaskForm, TaskUpdateForm, TaskSearchForm
from backend.tasks.models import Task
from backend.categories.models import Categories
from backend.utils import without_keys
from datetime import datetime

bp = Blueprint('tasks', __name__)

priority_name_dict = dict(priority_choice)
difficulty_name_dict = dict(task_difficulty_choice)
task_status_choices = TaskStatusTypes.choices()


def get_visible_treatments():
    if current_user.is_admin:
        treatments = Treatment.scan()
    else:
        treatments = Treatment.company_index.query(current_user.company_id)
    return treatments


@bp.route('')
@login_required
@is_company_user
def home():
    company_id = current_user.company_id
    treatments = get_visible_treatments()

    treatment_options = [('', '')]

    for obj in treatments:
        treatment_options.append((obj.id, obj.name))

    search_form = TaskSearchForm(request.args)
    status_options = TaskStatusTypes.choices()

    search_form.treatment.choices = treatment_options
    search_form.status.choices = (('', ''),) + status_options
    search_form.priority.choices = [('', '')] + priority_choice

    total_count = Task.count()

    return render_template('task/index.html', tasks=[], task_status_types=TaskStatusTypes.choices(),
                           search_form=search_form)


@bp.route('/add', methods=['GET', 'POST'])
@login_required
@is_company_user
def add():
    def set_choices(_form):
        _form.treatment_id.choices = treatment_choices
        _form.category.choices = category_choices
        _form.sub_category.choices = sub_category_choices
        _form.difficulty.choices = task_difficulty_choice
        _form.assigned_to.choices = user_choices
        _form.assigned_to.data = current_user.username
        _form.validate_by.choices = user_choices
        _form.priority.choices = priority_choice

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))

    treatments = Treatment.company_index.query(current_user.company_id)
    treatment_choices = [('', '')]
    for obj in treatments:
        treatment_choices.append((obj.id, obj.name))

    categories = Categories.scan()
    category_list = ['']
    category_choices = [('', '')]
    sub_category_choices = []
    for obj in categories:
        category = obj.category
        # sub_category = obj.sub_category
        if category not in category_list:
            category_choices.append((category, gettext(category)))
            category_list.append(category)
        # if category == category_list[0]:
        #     sub_category_choices.append((sub_category, gettext(sub_category)))

    form = ManualTaskForm()
    set_choices(form)

    if form.validate_on_submit():
        form_data = without_keys(form.data, ['csrf_token', 'attachment'])

        if datetime.now() > form_data.get('deadline'):
            flash(gettext(u'Please type in valid deadline'), 'alert alert-success')
            return render_template('task/add.html', form=form)

        sub_category_list = form_data.get('sub_category')
        form_data.update({'sub_category': ",".join(sub_category_list)})

        task = Task(**form_data)
        task.id = IDGenerator.generate_id(Task.Meta.table_name)
        task.company_id = current_user.company_id
        task.updated_at = datetime.now()
        task.status = TaskStatusTypes.OPEN.name
        task.attachments = []

        if form.attachment.data:
            f = form.attachment.data
            filename = secure_filename(f.filename)
            filepath = os.path.join(app.config['UPLOAD_PATH'], filename)
            f.save(filepath)
            task.attachments.append(filename)

        task.save()
        flash(gettext('Successfully added task'), 'alert alert-success')
        return redirect(url_for('tasks.home'))
    elif form.is_submitted():
        print form.errors
        flash(gettext('Please type in valid information'), 'alert alert-danger')

    return render_template('task/add.html', form=form, lang_code=g.lang_code)


@bp.route('/<task_id>/view')
@login_required
@is_company_user
def view(task_id):
    task_id = int(task_id)
    try:
        task = Task.get(task_id)
    except:
        return redirect(url_for('error_404'))

    if task.priority:
        task.priority_name = dict(priority_choice)[task.priority]
    if task.difficulty:
        task.difficulty_name = dict(task_difficulty_choice)[task.difficulty]
    if task.category:
        task.category_name = gettext(task.category)
    if task.sub_category:
        sub_categories = task.sub_category.split(',')
        sub_category_string_array = []
        for subCategory in sub_categories:
            sub_category_string_array.append(gettext(subCategory))
        task.sub_category_name = ",".join(sub_category_string_array)

    return render_template('task/view.html', task=task)


@bp.route('/<task_id>/update', methods=['GET', 'POST'])
@login_required
@is_company_user
def update(task_id):
    task_id = int(task_id)

    def set_choices(_form):
        _form.treatment_id.choices = treatment_choices
        _form.category.choices = category_choices
        _form.sub_category.choices = sub_category_choices
        _form.sub_category.data = selected_sub_categories
        _form.assigned_to.choices = user_choices
        _form.validate_by.choices = user_choices
        _form.priority.choices = priority_choice
        _form.difficulty.choices = task_difficulty_choice

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))

    treatments = Treatment.company_index.query(current_user.company_id)
    treatment_choices = []
    for obj in treatments:
        treatment_choices.append((obj.id, obj.name))

    task = Task.get(task_id)

    categories = Categories.scan()
    category_list = ['']
    category_choices = [('', '')]
    sub_category_choices = []
    for obj in categories:
        category = obj.category
        sub_category = obj.sub_category
        if category not in category_list:
            category_choices.append((category, gettext(category)))
            category_list.append(category)
        if category == task.category:
            sub_category_choices.append((sub_category, gettext(sub_category)))

    selected_sub_categories = []
    if task.sub_category:
        selected_sub_categories = task.sub_category.split(',')

    form = TaskUpdateForm()
    set_choices(form)

    if form.validate_on_submit():
        for key, value in form.data.items():
            if key == 'sub_category':
                value = ",".join(value)
            setattr(task, key, value)

        task.updated_at = datetime.now()

        task.attachments = []
        if form.update_attachment.data:
            task.attachments = []
            if form.attachment.data:
                f = form.attachment.data
                filename = secure_filename(f.filename)
                filepath = os.path.join(app.config['UPLOAD_PATH'], filename)
                f.save(filepath)
                task.attachments.append(filename)

        task.save()
        flash(gettext('Successfully updated the task'), 'alert alert-success')
        return redirect(url_for('tasks.home'))
    elif form.is_submitted():
        flash(gettext('Please type in valid information'), 'alert alert-success')
    else:
        form = TaskUpdateForm(obj=task)
        set_choices(form)

    return render_template('task/update.html', form=form, task_id=task_id, lang_code=g.lang_code)


@bp.route('/<task_id>/delete')
@login_required
@is_company_user
def delete(task_id):
    task_id = int(task_id)
    try:
        task = Task.get(task_id)
    except:
        return redirect(url_for('error_404'))

    task.delete()

    return redirect(url_for('tasks.home'))


@bp.route('/<task_id>/export', methods=['GET'])
@login_required
def export(task_id):
    try:
        task = Task.get(int(task_id))
    except:
        return redirect(url_for('error_404'))

    if not task:
        return abort(404)

    try:
        export_path = app.config['EXPORT_PATH']
        filename = 'task_' + task_id + '.csv'
        filepath = os.path.join(export_path, filename)
        difficulty_name = ''
        priority_name = ''

        if task.priority:
            priority_name = dict(priority_choice)[task.priority]

        if task.difficulty:
            difficulty_name = dict(task_difficulty_choice)[task.difficulty]

        try:
            treatment = Treatment.get(task.treatment_id)
        except:
            treatment = None

        attachments = len(task.attachments)

        with open(filepath, mode='w') as fobj:
            tr_writer = csv.writer(fobj, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            tr_writer.writerow(['Treatment', 'Name', 'Category', 'Description', 'Difficulty', 'Status',
                                'Updated At', 'Assignee', 'Validator', 'Priority', 'Deadline', 'Attachments'])
            tr_writer.writerow([treatment.name, task.name, task.category, task.description,
                                difficulty_name, task.status, task.updated_at.strftime('%m/%d/%Y'), task.assigned_to,
                                task.validate_by,
                                priority_name, task.deadline.strftime('%m/%d/%Y'), attachments])

        with io.open(filepath, mode="r", encoding='utf-8') as fobj:
            return Response(
                fobj.read(),
                mimetype="text/csv",
                headers={"Content-disposition": "attachment; filename={}".format(filename)})
    except Task.DoesNotExist:
        return render_template('404.html')


@bp.route('/export_all', methods=['GET'])
@login_required
def export_all():
    try:
        export_path = app.config['EXPORT_PATH']
        filename = 'tasks' + '.csv'
        filepath = os.path.join(export_path, filename)

        with open(filepath, mode='w') as fobj:
            tr_writer = csv.writer(fobj, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            tr_writer.writerow(['Treatment', 'Name', 'Category', 'Description', 'Difficulty', 'Status',
                                'Updated At', 'Assignee', 'Validator', 'Priority', 'Deadline', 'Attachments'])

            if current_user.is_admin:
                tasks = Task.scan()
            else:
                tasks = Task.company_index.query(current_user.company_id)

            difficulty_name = ''
            priority_name = ''

            for task in tasks:
                if task.priority:
                    priority_name = dict(priority_choice)[task.priority]

                if task.difficulty:
                    difficulty_name = dict(task_difficulty_choice)[task.difficulty]

                try:
                    treatment = Treatment.get(task.treatment_id)
                except:
                    treatment = None

                attachments = len(task.attachments)
                tr_writer.writerow([treatment.name if treatment is not None else '',
                                    task.name,
                                    task.category,
                                    task.description,
                                    difficulty_name, task.status, task.updated_at.strftime('%m/%d/%Y'),
                                    task.assigned_to, task.validate_by,
                                    priority_name, task.deadline.strftime('%m/%d/%Y'), attachments])

        with io.open(filepath, mode="r", encoding='utf-8') as fobj:
            return Response(
                fobj.read(),
                mimetype="text/csv",
                headers={"Content-disposition": "attachment; filename={}".format(filename)})
    except Task.DoesNotExist:
        return render_template('404.html')


@bp.route('/task_list', methods=['POST'])
@login_required
def task_list():
    last_evaluated_key = request.json.get('last_evaluated_key')

    search_name = request.json.get('name')
    search_treatment = request.json.get('treatment')
    search_status = request.json.get('status')
    # search_priority = request.json.get('priority')

    filter_condition = None

    if search_name:
        condition = (Task.name.contains(search_name))
        if filter_condition is not None:
            filter_condition = filter_condition & condition
        else:
            filter_condition = condition

    if search_treatment:
        condition = (Task.treatment_id == search_treatment)
        if filter_condition is not None:
            filter_condition = filter_condition & condition
        else:
            filter_condition = condition

    if search_status:
        condition = (Task.status == search_status)
        if filter_condition is not None:
            filter_condition = filter_condition & condition
        else:
            filter_condition = condition

    # if search_priority:
    #     search_priority = int(search_priority)
    #     condition = (Task.priority == search_priority)
    #     if filter_condition is not None:
    #         filter_condition = filter_condition & condition
    #     else:
    #         filter_condition = condition

    if current_user.is_admin:
        results = Task.scan(
            filter_condition,
            limit=5,
            last_evaluated_key=last_evaluated_key
        )

    else:
        results = Task.company_index.query(
            current_user.company_id,
            filter_condition,
            scan_index_forward=False,
            last_evaluated_key=last_evaluated_key,
            limit=5)

    task_objects = []
    for task in results:
        try:
            task.treatment = Treatment.get(task.treatment_id)
        except:
            task.treatment = None

        if task.priority:
            task.priority_name = priority_name_dict[task.priority]
        if task.difficulty:
            task.difficulty_name = difficulty_name_dict[task.difficulty]

        task_objects.append(task)

    last_evaluated_key = results.last_evaluated_key

    return render_template('task/task_list.html',
                           last_evaluated_key=json.dumps(last_evaluated_key),
                           items=task_objects,
                           task_status_types=task_status_choices
                           )
