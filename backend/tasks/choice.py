from backend.enums import ChoiceEnum
from flask_babel import gettext, lazy_gettext

priority_choice = [(1, lazy_gettext('High')), (2, lazy_gettext('Critical')), (3, lazy_gettext('Medium')), (4, lazy_gettext('Normal')), (5, lazy_gettext('Low'))]
task_difficulty_choice = [(1, lazy_gettext('Strong')), (2, lazy_gettext('Average')), (3, lazy_gettext('Normal')), (4, lazy_gettext('Low'))]


class CategoryChoice(ChoiceEnum):
    SECURE = lazy_gettext('Secure')
    SEVERE = lazy_gettext('Severe')
    ENCRYPTION = lazy_gettext('Encryption')


class TaskStatusTypes(ChoiceEnum):
    OPEN = lazy_gettext('Open')
    IN_PROGRESS = lazy_gettext('In Progress')
    PLANNED = lazy_gettext('Planned')
    COMPLETED = lazy_gettext('Completed')
    VALIDATED = lazy_gettext('Validated')