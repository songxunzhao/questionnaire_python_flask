import os

from flask import current_app as app
from pynamodb.attributes import NumberAttribute, UnicodeAttribute, UTCDateTimeAttribute, BooleanAttribute, ListAttribute
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection, KeysOnlyProjection, IncludeProjection
from pynamodb.models import Model

from backend.models import BaseModel

class TaskCompanyIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'task-company-index'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    company_id = NumberAttribute(hash_key=True)


class TaskTreatmentIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'task-treatment-index'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    treatment_id = UnicodeAttribute(hash_key=True)


class Task(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'task'

    id = NumberAttribute(hash_key=True)
    treatment_id = UnicodeAttribute()
    company_id = NumberAttribute()
    name = UnicodeAttribute()
    description = UnicodeAttribute(null=True)
    category = UnicodeAttribute(null=True)
    sub_category = UnicodeAttribute(null=True)
    difficulty = NumberAttribute()
    status = UnicodeAttribute()
    updated_at = UTCDateTimeAttribute()
    assigned_to = UnicodeAttribute()
    validate_by = UnicodeAttribute()
    priority = NumberAttribute()
    deadline = UTCDateTimeAttribute()
    attachments = ListAttribute()

    company_index = TaskCompanyIndex()
    treatment_index = TaskTreatmentIndex()

    @property
    def attachment_urls(self):
        result = []
        for attachment in self.attachments:
            if attachment.startswith('http://') or attachment.startswith('https://'):
                result.append({
                    'name': os.path.basename(attachment),
                    'absolute_url': attachment
                })
            else:
                result.append({
                    'name': attachment,
                    'absolute_url': os.path.join(app.config.get('UPLOAD_URL', ''), attachment)
                })
                print result

        return result
