from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import SelectField, StringField, DateTimeField, BooleanField, validators, SelectMultipleField
from wtforms.widgets import TextArea

class NoValidationSelectMultipleField(SelectMultipleField):
    def pre_validate(self, form):
         """per_validation is disabled"""

class ManualTaskForm(FlaskForm):
    treatment_id = SelectField(lazy_gettext('Treatment'), validators=[validators.DataRequired()])
    name = StringField(lazy_gettext('Name'), validators=[validators.DataRequired()])
    description = StringField(lazy_gettext('Description'), widget=TextArea())
    category = SelectField(lazy_gettext('Category'), validators=[validators.DataRequired()])
    sub_category = NoValidationSelectMultipleField(lazy_gettext('Sub Category'), validators=[validators.DataRequired()])
    difficulty = SelectField(lazy_gettext('Difficulty'), coerce=int)
    assigned_to = SelectField(lazy_gettext('Assign To'), validators=[validators.DataRequired()])
    validate_by = SelectField(lazy_gettext('Validate By'))
    priority = SelectField(lazy_gettext('Priority'), coerce=int)
    deadline = DateTimeField(lazy_gettext('Deadline'), format='%m/%d/%Y')
    attachment = FileField(lazy_gettext('Attachments'))


class TaskForm(FlaskForm):
    name = StringField(lazy_gettext('Name'), validators=[validators.DataRequired()])
    description = StringField(lazy_gettext('Description'), widget=TextArea())
    category = SelectField(lazy_gettext('Category'))
    sub_category = NoValidationSelectMultipleField(lazy_gettext('Sub Category'))
    assigned_to = SelectField(lazy_gettext('Assign To'), validators=[validators.DataRequired()])
    validate_by = SelectField(lazy_gettext('Validate By'))
    priority = SelectField(lazy_gettext('Priority'), coerce=int)
    deadline = DateTimeField(lazy_gettext('Deadline'), format='%m/%d/%Y')
    attachment = FileField(lazy_gettext('Attachments'))


class TaskUpdateForm(ManualTaskForm):
    update_attachment = BooleanField(lazy_gettext('Update attachment?'))

class TaskSearchForm(FlaskForm):
    name = StringField(lazy_gettext('Name'), default='', validators=[validators.optional()])
    treatment = SelectField(lazy_gettext('Treatment'), default='', validators=[validators.optional()])
    status = SelectField(lazy_gettext('Status'), default='', validators=[validators.optional()])
    priority = SelectField(lazy_gettext('Priority'), default='', validators=[validators.optional()])
