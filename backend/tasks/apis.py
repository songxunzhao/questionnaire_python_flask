import datetime
import os

from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, jsonify, abort, current_app as app
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )
from werkzeug.utils import secure_filename

from backend.assignments.choice import TreatmentStatusTypes
from backend.assignments.models import Treatment
from backend.auth.models import AuthUser
from backend.contractors.models import Contractor
from backend.decorators import is_company_user, is_dpo
from backend.models import IDGenerator
from backend.tasks.choice import CategoryChoice, priority_choice, TaskStatusTypes, task_difficulty_choice
from backend.tasks.forms import ManualTaskForm
from backend.tasks.models import Task
from backend.utils import total_questions, num_answers, without_keys

bp = Blueprint('api.tasks', __name__)


@bp.route('/add', methods=['POST'])
@login_required
@is_company_user
def add():
    treatment_id = request.json.get('treatment_id')
    name = request.json.get('name')
    description = request.json.get('description')
    assigned_to = request.json.get('assigned_to')
    validate_by = request.json.get('validate_by')

    if not treatment_id or not name:
        return jsonify({
            'error': 'Bad request'
        }), 404

    task = Task()
    task.id = IDGenerator.generate_id(Task.Meta.table_name)
    task.company_id = current_user.company_id
    task.updated_at = datetime.datetime.now()
    task.priority = 5
    task.difficulty = 3
    task.deadline = datetime.datetime.now() + datetime.timedelta(days=10)
    task.status = TaskStatusTypes.OPEN.name
    task.assigned_to = assigned_to
    task.validate_by = validate_by
    task.name = name
    task.description = description
    task.treatment_id = treatment_id
    task.attachments = []

    task.save()
    print '+++++++++++'
    print 'add task: python'
    return jsonify(task.to_dict())


@bp.route('/<task_id>/status', methods=['POST'])
@login_required
@is_dpo
def set_status(task_id):
    task_id = int(task_id)
    try:
        task = Task.get(task_id)
        task.status = request.json.get('status')
        task.save()
        return jsonify(task.to_dict())
    except Task.DoesNotExist:
        return abort(404)
