from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, jsonify, abort, g
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )
from flask_babel import gettext, lazy_gettext

from backend.categories.models import Categories

bp = Blueprint('api.categories', __name__)

@bp.route('/get_sub', methods=['POST'])
@login_required
def get_sub():
    sub_category_list = []

    category = request.form.get('category')
    if category == '':
        return jsonify(sub_category_list)

    if not category:
        return abort(404)

    categories = Categories.scan()

    g.lang_code = request.form.get('lang')

    origin_list = []

    for obj in categories:
        if obj.category == category:
            origin_list.append(obj.sub_category)
            sub_category_list.append(gettext(obj.sub_category))

    print '--------------'
    print origin_list
    print sub_category_list
    print g.lang_code


    return jsonify(sub_category_list)



