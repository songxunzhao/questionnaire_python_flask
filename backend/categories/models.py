from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UTCDateTimeAttribute, BooleanAttribute,
    JSONAttribute)

from backend.models import BaseModel


class Categories(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'category'
        

    id = NumberAttribute(hash_key=True)
    category = UnicodeAttribute(null=False)
    sub_category = UnicodeAttribute(null=False)
