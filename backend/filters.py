# Jinja 2 template filters


def register_filters(app):
    @app.template_filter('datetimeformat')
    def datetimeformat(value, format='%H:%M / %d-%m-%Y'):
        if value is not None:
            return value.strftime(format)
        else:
            return ''
