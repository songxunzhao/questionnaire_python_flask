from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, abort, current_app as app
from flask_babel import lazy_gettext
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )

from backend.auth.choice import RoleTypes
from backend.companies.models import Company
from backend.contractors.forms import ContractorForm, ContractorFormBase
from backend.contractors.models import Contractor
from backend.decorators import is_company_user
from backend.models import IDGenerator
from backend.utils import without_keys

import os
import csv

bp = Blueprint('contractors', __name__)


@bp.route('')
@login_required
def home():
    contractors = Contractor.scan()
    contractor_list = []

    for contractor in contractors:
        if not current_user.is_admin and contractor.company_id != current_user.company_id:
            continue
        # if contractor.company_id:
        #     try:
        #         company = Company.get(contractor.company_id)
        #         contractor.company_name = company.name
        #     except Company.DoesNotExist:
        #         contractor.company_name = None

        contractor.role = RoleTypes[contractor.role].value

        contractor_list.append(contractor)
    return render_template('contractor/index.html', contractors=contractor_list)


@bp.route('/delete/<contractor_company>/<contractor_name>')
@login_required
def delete(contractor_company, contractor_name):
    contractors = Contractor.query(contractor_name, Contractor.company_id ==int(contractor_company))
    for contractor in contractors:
        contractor.delete()
    return redirect(url_for('contractors.home'))


@bp.route('/add', methods=['GET', 'POST'])
@login_required
def add():
    form = ContractorForm()
    # choices = []
    # if current_user.is_admin:
    #     company_set = Company.scan()
    #     for company in company_set:
    #         choices.append((company.id, company.name, ))
    # else:
    #     current_company_id = current_user.company_id
    #     companies = list(Company.query(current_company_id))
    #
    #     if len(companies) > 0:
    #         choices.append((current_company_id, companies[0].name))

    # form.company_name.choices = choices

    # TODO: check request type
    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])
        data['role'] = RoleTypes.DATA_MONITOR.name
        password = data.pop('password')

        contractor = Contractor(**data)
        contractor.password = password

        if not current_user.is_admin:
            contractor.company_id = current_user.company_id

        contractor.save()
        flash(lazy_gettext("Contractor was added successfully"), "alert alert-success")
        return redirect(url_for('contractors.home'))
    elif form.is_submitted():
        print form.errors
        flash(lazy_gettext("Please type in valid information"), "alert alert-success")

    return render_template('contractor/add.html', form=form)


@bp.route('/update/<contractor_name>', methods=['GET', 'POST'])
@login_required
@is_company_user
def update(contractor_name):
    # TODO: check request type
    contractors = list(Contractor.query(contractor_name))

    if len(contractors) == 0:
        redirect('404')
        return

    contractor = contractors[0]

    form = ContractorFormBase()
    # choices = []
    # company_set = Company.scan()
    # for company in company_set:
    #     choices.append((company.id, company.name))
    #
    # form.company_id.choices = choices

    if form.validate_on_submit():
        for key, value in form.data.items():
            setattr(contractor, key, value)

        contractor.save()
        flash(lazy_gettext("Contractor was saved successfully"), "alert alert-success")
        return redirect(url_for('contractors.home'))
    elif form.is_submitted():
        flash(lazy_gettext("Please type in valid information"), "alert alert-danger")
    else:
        form = ContractorFormBase(obj=contractor)
        # form.company_name.choices = choices

    return render_template('contractor/update.html', form=form, contractor=contractor)


@bp.route('/export/<contractor_name>', methods=['GET'])
@login_required
def export(contractor_name):
    # TODO: check request type
    contractors = list(Contractor.query(contractor_name))

    if len(contractors) == 0:
        redirect('404')
        return

    contractor = contractors[0]

    try:
        export_path = app.config['EXPORT_PATH']
        filename = 'contractor_' + contractor_name + '.csv'
        filepath = os.path.join(export_path, filename)

        company_name = Company.get(contractor.company_id).name

        with open(filepath, 'wb') as fobj:
            tr_writer = csv.writer(fobj, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            tr_writer.writerow(['FirstName', 'LastName', 'Company', 'Phone', 'Email', 'Address',
                                'ZipCode', 'City', 'Role', 'Confirmity'])
            tr_writer.writerow([contractor.first_name, contractor.last_name,
                                company_name, contractor.phone, contractor.email, contractor.address,
                                contractor.zipcode, contractor.city, contractor.role, contractor.in_conformity])

        with open(filepath, "r") as fobj:
            return Response(
                fobj.read(),
                mimetype="text/csv",
                headers={"Content-disposition": "attachment; filename={}".format(filename)})
    except:
        return render_template('404.html')


@bp.route('/export_all', methods=['GET'])
@login_required
def export_all():
    try:
        export_path = app.config['EXPORT_PATH']
        filename = 'contractors' + '.csv'
        filepath = os.path.join(export_path, filename)

        with open(filepath, 'wb') as fobj:
            tr_writer = csv.writer(fobj, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            tr_writer.writerow(['FirstName', 'LastName', 'Company', 'Phone', 'Email', 'Address',
                                'ZipCode', 'City', 'Role', 'Confirmity'])

            if current_user.is_admin:
                contractors = Contractor.scan()
            else:
                contractors = Contractor.company_index.query(current_user.company_id)

            for contractor in contractors:
                company_name = Company.get(contractor.company_id).name

                tr_writer.writerow([contractor.first_name, contractor.last_name,
                                    company_name, contractor.phone, contractor.email, contractor.address,
                                    contractor.zipcode, contractor.city, contractor.role, contractor.in_conformity])

        with open(filepath, "r") as fobj:
            return Response(
                fobj.read(),
                mimetype="text/csv",
                headers={"Content-disposition": "attachment; filename={}".format(filename)})
    except Exception as e:
        print e.message
        return render_template('404.html')
