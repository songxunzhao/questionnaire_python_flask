from pynamodb.indexes import GlobalSecondaryIndex, AllProjection, KeysOnlyProjection, IncludeProjection
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UnicodeSetAttribute, UTCDateTimeAttribute, BooleanAttribute,
    JSONAttribute)
from werkzeug.security import generate_password_hash, check_password_hash

from backend.models import BaseModel


class ContractorCompanyIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'contractor-company-index'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    company_id = NumberAttribute(hash_key=True)


class Contractor(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'contractor'

    first_name = UnicodeAttribute()
    last_name = UnicodeAttribute()
    company_id = NumberAttribute(null=True, range_key=True)
    phone = UnicodeAttribute()
    email = UnicodeAttribute(hash_key=True)
    address = UnicodeAttribute()
    zipcode = UnicodeAttribute()
    city = UnicodeAttribute()
    in_conformity = BooleanAttribute()
    confidentiality_clause = BooleanAttribute()
    password_hash = UnicodeAttribute()
    role = UnicodeAttribute()
    company_name = UnicodeAttribute(null=True)
    web_site = UnicodeAttribute(null=True)

    company_index = ContractorCompanyIndex()

    @property
    def password(self):
        raise AttributeError(u"Password is not a readable attribute.")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<Contractor %s>" % self.email or "None"
