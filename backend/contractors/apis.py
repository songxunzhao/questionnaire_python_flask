from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, jsonify
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )

from backend.contractors.models import Contractor
from backend.decorators import is_company_user

bp = Blueprint('api.contractors', __name__)


@bp.route('/')
@login_required
@is_company_user
def get_all():
    # company_id = current_user.company_id
    # contractors = Contractor.company_index.query(company_id)
    contractors = Contractor.scan()
    result = []

    for contractor in contractors:
        if contractor.company_name:
            result.append({
                'value': contractor.company_name,
                'text': contractor.company_name
            })

    return jsonify(result)
