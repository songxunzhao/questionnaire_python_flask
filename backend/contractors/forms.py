from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import TextField, StringField, SelectField, BooleanField, PasswordField, validators


class ContractorFormBase(FlaskForm):
    first_name = StringField(lazy_gettext('First name'), [validators.required()])
    last_name = StringField(lazy_gettext('Last name'), [validators.required()])
    # company_name = SelectField(lazy_gettext('Company'), [validators.optional()], coerce=int, default='')
    phone = StringField(lazy_gettext('Phone number'), [validators.required()])
    email = StringField(lazy_gettext('Email'), [validators.required(), validators.email()])
    address = StringField(lazy_gettext('Address'), [validators.required()])
    zipcode = StringField(lazy_gettext('Zip code'), [validators.required()])
    city = StringField(lazy_gettext('City'), [validators.required()])
    in_conformity = BooleanField(lazy_gettext('RGPD conformity'))
    confidentiality_clause = BooleanField(lazy_gettext('Do you have a confidentiality clause contract?'));
    company_name = StringField(lazy_gettext('Name of Company'), [validators.required()])
    web_site = StringField(lazy_gettext('Web site'))


class ContractorForm(ContractorFormBase):
    password = PasswordField(lazy_gettext('Password'))
