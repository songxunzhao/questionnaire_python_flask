import datetime
import string
import random
import pytz

from flask_babel import gettext
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )
from flask import render_template, flash, url_for, \
    redirect, request, Blueprint, g
import logging

from backend.utils import without_keys
from .forms import LoginForm, UserSettingForm, UserChangePasswordForm
from .models import AuthUser
from .login_manager import login_manager

"""User login management."""

bp = Blueprint('auth', __name__)


@login_manager.user_loader
def load_user(user_id):
    logging.debug("Querying users.")
    try:
        user = AuthUser.get(user_id)
        return user
    except:
        return None


def get_random_part(length):
    """Get a random 5 letter string."""
    random_pad = "".join(
        random.choice(string.ascii_lowercase) for i in range(length))
    return random_pad


@bp.route("/login", methods=["GET", "POST"])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for("lang_index"))
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = AuthUser.get(form.username.data)
        except AuthUser.DoesNotExist:
            user = None

        if user is not None and user.verify_password(form.password.data):
            if user.expiration_date and user.expiration_date < datetime.datetime.now(pytz.utc):
                flash(gettext(u"Your account was expired"), "alert alert-danger")
            else:
                login_user(user, form.remember_me.data)
                return redirect(request.args.get("next", "") or
                                url_for("lang_index"))
        else:
            flash(gettext(u"Invalid username and password"), "alert alert-danger")

    return render_template("auth/login.html", form=form)


@bp.route("/logout")
@login_required
def logout():
    # Clear language code setting from URL
    g.lang_code = None

    logout_user()
    return redirect(url_for('auth.login'))


@bp.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    form = UserSettingForm()
    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])
        for key, value in data.items():
            setattr(current_user, key, value)
        current_user.save()
        return redirect(url_for('index'))
    elif form.is_submitted():
        flash(gettext('Please type in valid information'), "alert alert-danger")
    else:
        form = UserSettingForm(obj=current_user)

    return render_template('auth/profile.html', form=form)


@bp.route('/change_password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = UserChangePasswordForm()

    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])
        if current_user.verify_password(data['old_password']):
            if data['new_password'] != data['confirm_password']:
                flash(gettext('Please confirm with your correct password'), 'alert alert-danger')
            else:
                current_user.password = data['new_password']
                current_user.save()
                return redirect(url_for('index'))
        else:
            flash(gettext('Old password is not correct'), 'alert alert-danger')
    elif form.is_submitted():
        flash(gettext('Please type in valid information'))

    return render_template('auth/change_password.html', form=form)

@bp.route("/support")
def support():
    return render_template("auth/support.html")

@bp.route("/help")
def help():
    return render_template("auth/help.html")


@bp.route('/set_language', methods=['GET'])
@login_required
def set_language():
    lang=request.args['lang']
    g.lang_code = lang
    return redirect(url_for('index', lang_code=lang))

@bp.route('/dashboard')
@login_required
def dashboard():
    if g.lang_code:
        return redirect(url_for('index', lang_code=g.lang_code))
    else:
        return redirect(url_for('lang_index'))
