from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, \
    BooleanField, DateTimeField, validators, SelectField
from wtforms.validators import DataRequired, Optional, Length
from wtforms import Label

"""UI Forms."""


class LoginForm(FlaskForm):
    username = StringField('', validators=[
        DataRequired(message=lazy_gettext("Provide a username."))])
    password = PasswordField('', validators=[
        DataRequired(message=lazy_gettext("Provide a password."))])
    remember_me = BooleanField(lazy_gettext(u"Remember Me"))


class AdminUserCreateForm(FlaskForm):
    username = StringField(lazy_gettext('Username'),
                           validators=[DataRequired(message=lazy_gettext('Provide a username'))])
    first_name = StringField(lazy_gettext('First name'))
    last_name = StringField(lazy_gettext('Last name'))
    phone = StringField(lazy_gettext('Phone number'))
    email = StringField(lazy_gettext('Email'))
    licence = StringField(lazy_gettext('Licence'))
    expiration_date = DateTimeField(lazy_gettext('Expiration Date'), format='%m/%d/%Y')
    services_included = StringField(lazy_gettext('Services included'))
    password = PasswordField(lazy_gettext('Password'),
                             validators=[DataRequired(message=lazy_gettext('Password is required'))])
    role = SelectField(lazy_gettext('Role'), choices=[],
                       validators=[DataRequired(message=lazy_gettext("Role is required"))])
    user_type = SelectField(lazy_gettext('Type of User'), choices=[],
                       validators=[DataRequired(message=lazy_gettext("Type of user is required"))])

class AdminUserUpdateForm(FlaskForm):
    username = StringField(lazy_gettext('Username'),
                           validators=[DataRequired(message=lazy_gettext('Provide a username.'))])
    first_name = StringField(lazy_gettext('First name'))
    last_name = StringField(lazy_gettext('Last name'))
    phone = StringField(lazy_gettext('Phone number'))
    email = StringField(lazy_gettext('Email'))
    licence = StringField(lazy_gettext('Licence'))
    expiration_date = DateTimeField(lazy_gettext('Expiration Date'), format='%m/%d/%Y')
    services_included = StringField(lazy_gettext('Services included'))
    role = SelectField(lazy_gettext('Role'), choices=[],
                       validators=[DataRequired(message=lazy_gettext("Role is required"))])
    user_type = SelectField(lazy_gettext('Type of User'), choices=[],
                           validators=[DataRequired(message=lazy_gettext("Type of user is required"))])


class UserCreateForm(FlaskForm):
    username = StringField(lazy_gettext('Username'),
                           validators=[DataRequired(message=lazy_gettext('Provide a username.'))])
    first_name = StringField(lazy_gettext('First name'))
    last_name = StringField(lazy_gettext('Last name'))
    phone = StringField(lazy_gettext('Phone number'))
    email = StringField(lazy_gettext('Email'))
    licence = StringField(lazy_gettext('Licence'))
    expiration_date = DateTimeField(lazy_gettext('Expiration Date'), format='%m/%d/%Y')
    services_included = SelectField(lazy_gettext('Services included'))
    password = PasswordField(lazy_gettext('Password'),
                             validators=[DataRequired(message=lazy_gettext('Password field is required'))])
    role = SelectField(lazy_gettext('Role'), choices=[],
                       validators=[DataRequired(message=lazy_gettext("Role field is required"))])
    user_type = SelectField(lazy_gettext('Type of User'), choices=[],
                           validators=[DataRequired(message=lazy_gettext("Type of user is required"))])
    company_id = SelectField(lazy_gettext('Company'), choices=[], coerce=int,
                             validators=[DataRequired(message=lazy_gettext('Company field is required'))])


class UserUpdateForm(FlaskForm):
    first_name = StringField(lazy_gettext('First name'))
    last_name = StringField(lazy_gettext('Last name'))
    phone = StringField(lazy_gettext('Phone number'))
    email = StringField(lazy_gettext('Email'))
    licence = StringField(lazy_gettext('Licence'))
    expiration_date = DateTimeField(lazy_gettext('Expiration Date'), format='%m/%d/%Y')
    services_included = StringField(lazy_gettext('Services included'))
    role = SelectField(lazy_gettext('Role'), choices=[], validators=[DataRequired(message="Role is required")])
    user_type = SelectField(lazy_gettext('Type of User'), choices=[],
                           validators=[DataRequired(message=lazy_gettext("Type of user is required"))])
    company_id = SelectField(lazy_gettext('Company'), choices=[], coerce=int,
                             validators=[DataRequired(message=lazy_gettext('Company field is required'))])


class UserAssignRoleForm(FlaskForm):
    username = SelectField(lazy_gettext('User'),
                           validators=[DataRequired(message=lazy_gettext('User is required'))])
    role = SelectField(lazy_gettext('Role'), choices=[],
                       validators=[DataRequired(message=lazy_gettext('Role is required'))])


class SearchUserForm(FlaskForm):
    username = StringField(lazy_gettext('Username'), default='', validators=[Optional()])


class UserSettingForm(FlaskForm):
    first_name = StringField(lazy_gettext('First name'))
    last_name = StringField(lazy_gettext('Last name'))
    phone = StringField(lazy_gettext('Phone number'))
    email = StringField(lazy_gettext('Email'))
    # licence = StringField(lazy_gettext('Licence'))
    services_included = StringField(lazy_gettext('Services included'))


class UserChangePasswordForm(FlaskForm):
    old_password = PasswordField(lazy_gettext('Old Password', validators=[DataRequired()]))
    new_password = PasswordField(lazy_gettext('New Password', validators=[DataRequired(), Length(min=6)]))
    confirm_password = PasswordField(lazy_gettext('Confirm Password', validators=[DataRequired(), Length(min=6)]))

class DashboardSearchForm(FlaskForm):
    from_date = DateTimeField('', format='%m/%d/%Y')
    to_date = DateTimeField('', format='%m/%d/%Y')
    search_check = BooleanField('')