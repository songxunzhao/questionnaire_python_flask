from flask_login import UserMixin
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection, KeysOnlyProjection
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UnicodeSetAttribute, UTCDateTimeAttribute, BooleanAttribute,
    ListAttribute)
from werkzeug.security import generate_password_hash, \
    check_password_hash

from backend.auth.choice import UserRoleTypes
from backend.models import BaseModel


class CompanyUserIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'company-user-index'
        read_capacity_units = 2
        write_capacity_units = 1
        projection = AllProjection()
    company_id = NumberAttribute(hash_key=True)


class AuthUser(BaseModel, UserMixin):
    class Meta(BaseModel.Meta):
        table_name = 'auth_users'

    username = UnicodeAttribute(hash_key=True)
    first_name = UnicodeAttribute(default='', null=True)
    last_name = UnicodeAttribute(default='', null=True)
    is_admin = BooleanAttribute(default=False)
    company_id = NumberAttribute()
    phone = UnicodeAttribute(null=True)
    email = UnicodeAttribute(null=True)
    licence = UnicodeAttribute(null=True)
    expiration_date = UTCDateTimeAttribute(null=True)
    services_included = UnicodeAttribute(null=True)
    password_hash = UnicodeAttribute(default='')
    role = UnicodeAttribute(null=True)
    user_type = UnicodeAttribute(null=True)

    company_index = CompanyUserIndex()

    def get_id(self):
        try:
            return self.username
        except AttributeError:
            raise NotImplementedError('No `id` attribute - override `get_id`')

    @property
    def password(self):
        raise AttributeError(u"Password is not a readable attribute.")

    @property
    def is_dpo(self):
        return self.role == UserRoleTypes.DPO.name

    @property
    def is_data_manager(self):
        print self.role
        print UserRoleTypes.DATA_MANAGER.name
        return self.role == UserRoleTypes.DATA_MANAGER.name

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User %s>" % self.username or "None"


class UserService(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'user_service'

    id = NumberAttribute(hash_key=True)
    user_id = NumberAttribute()
    service_id = NumberAttribute()


class Service(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'service'

    id = NumberAttribute(hash_key=True)
    fr = UnicodeAttribute()

