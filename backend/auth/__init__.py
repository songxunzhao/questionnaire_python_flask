from .login_manager import login_manager
from .views import bp


def init_app(app, url_prefix='/auth'):
    login_manager.init_app(app)
    app.register_blueprint(bp, url_prefix='/<lang_code>' + url_prefix)
    app.register_blueprint(bp, url_prefix=url_prefix)
