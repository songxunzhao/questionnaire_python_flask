from backend.enums import ChoiceEnum
from flask_babel import gettext, lazy_gettext


class UserRoleTypes(ChoiceEnum):
    DPO = lazy_gettext('Company manager')
    DATA_MANAGER = lazy_gettext('Data manager')
    DATA_USER = lazy_gettext('Data user')
    DATA_MONITOR = lazy_gettext('Data monitor')


class UserTypes(ChoiceEnum):
    LAWYER = lazy_gettext('Lawyer')
    EXTERNAL = lazy_gettext('External')
    CONTRACTOR = lazy_gettext('Contractor')
    PROVIDER = lazy_gettext('Provider')
    AUDITOR = lazy_gettext('Auditor')
    CONSULTING = lazy_gettext('Consulting')
    INTERNAL = lazy_gettext('Internal')
    DPO = lazy_gettext('DPO')
    OTHERS = lazy_gettext('Others')
    MANAGER_OF_DATA = lazy_gettext('Manager of Data')

class TreatmentTypes(ChoiceEnum):
    EXPRESS_AUDIT = lazy_gettext('Express')
    THOROUGH_AUDIT = lazy_gettext('Thorough')
    USE_EXISTING_AUDIT = lazy_gettext('Existing audit')
    NONE_AUDIT = lazy_gettext('None')


class TreatmentDataSensibility(ChoiceEnum):
    GENETIC_DATA = 'Genetic Data'
    HEALTH_DATA = 'Health Data'
    SEX_LIFE = 'Data concerning sex life'
    CHILDREN = 'Data concerning children'


class TaskCategory(ChoiceEnum):
    ENCRYPTION = 'encryption'
    RIGHT_OF_RECTIFICATION = ' right of rectification'
    PERMISSION_TO_ACCESS = 'permission to access'
    LOG_COLLECTOR = 'log collector'
    SAFEGUARD = 'safeguard'


class RoleTypes(ChoiceEnum):
    DPO = 'DPO'
    DATA_MANAGER = 'Data Manager'
    DATA_USER = 'Data User'
    DATA_MONITOR = 'Data Monitor'
