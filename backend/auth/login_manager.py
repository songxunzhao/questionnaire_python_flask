from flask_login import LoginManager
from flask_babel import lazy_gettext

login_manager = LoginManager()

login_manager.session_protection = "strong"
login_manager.login_message_category = "info"
login_manager.login_message = lazy_gettext("You need to login.")
# login_manager.login_message = "You need to login."
login_manager.login_view = "auth.login"
