from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, abort, current_app as app
from flask_babel import lazy_gettext, gettext
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )

from backend.auth.choice import RoleTypes, UserRoleTypes
from backend.auth.forms import AdminUserCreateForm, AdminUserUpdateForm
from backend.auth.models import AuthUser
from backend.companies.forms import CompanyForm
from backend.companies.models import Company
from backend.contractors.forms import ContractorForm, ContractorFormBase
from backend.contractors.models import Contractor
from backend.decorators import is_admin
from backend.models import IDGenerator
from backend.utils import without_keys

bp = Blueprint('companies', __name__)


@bp.route('')
@login_required
@is_admin
def home():
    companies = Company.scan()

    return render_template('company/index.html', companies=companies)


@bp.route('/<company_id>/delete/')
@login_required
@is_admin
def delete(company_id):
    company = Company.get(int(company_id))
    if company:
        # Delete all users under the company
        user_objects = AuthUser.company_index.query(company.id)
        for user in user_objects:
            user.delete()

        company.delete()
        # Redirect to contractor list
        return redirect(url_for('companies.home'))
    else:
        # Redirect to contractor list
        return render_template('404.html'), 404


@bp.route('/add', methods=['GET', 'POST'])
@login_required
@is_admin
def add():
    form = CompanyForm()
    form.language.choices = app.config['SUPPORTED_LANGUAGES'].items()

    # TODO: check request type
    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])
        company = Company(**data)
        company.id = IDGenerator.generate_id(Company.Meta.table_name)
        company.save()
        flash(gettext("Company was created successfully"), "alert alert-success")
        return redirect(url_for('companies.home'))
    elif form.is_submitted():
        print form.errors
        flash(gettext("Please type in valid information"), "alert alert-success")

    return render_template('company/add.html', form=form)


@bp.route('/<company_id>/update', methods=['GET', 'POST'])
@login_required
@is_admin
def update(company_id):
    # TODO: check request type
    company = Company.get(int(company_id))
    if not company:
        return render_template('404.html'), 404

    form = CompanyForm()
    form.language.choices = app.config['SUPPORTED_LANGUAGES'].items()

    print app.config['SUPPORTED_LANGUAGES'].items()
    if form.validate_on_submit():
        for key, value in form.data.items():
            setattr(company, key, value)

        company.save()
        flash(gettext("Company was saved successfully"), "alert alert-success")
        return redirect(url_for('companies.home'))
    else:
        form = CompanyForm(obj=company)
        form.language.choices = app.config['SUPPORTED_LANGUAGES'].items()
    return render_template('company/update.html', form=form, company=company)


@bp.route('/<company_id>/users', methods=['GET', 'POST'])
@login_required
@is_admin
def users(company_id):
    company = Company.get(int(company_id))
    objects = AuthUser.company_index.query(int(company_id))
    
    return render_template('company/users.html', users=objects, company=company)


@bp.route('/<company_id>/users/add', methods=['GET', 'POST'])
@login_required
@is_admin
def add_user(company_id):
    form = AdminUserCreateForm()
    form.role.choices = UserRoleTypes.choices()

    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])
        data.update({'company_id': int(company_id)})

        password = data.pop('password')

        user = AuthUser(**data)
        user.password = password
        user.save()

        flash(gettext("User was created successfully"), "alert alert-success")
        return redirect(url_for('companies.users', company_id=company_id))
    elif form.is_submitted():
        flash(gettext("Please type in valid information"), "alert alert-success")

    return render_template('company/add_user.html', form=form, company_id=company_id)


@bp.route('/<company_id>/users/<user_id>/update', methods=['GET', 'POST'])
@login_required
@is_admin
def update_user(company_id, user_id):
    form = AdminUserUpdateForm()
    form.role.choices = UserRoleTypes.choices()

    user = AuthUser.get(user_id)

    if not user:
        return redirect('404')

    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])

        for key, value in data.items():
            setattr(user, key, value)

        user.save()
        flash(gettext("User was updated successfully"), "alert alert-success")
        return redirect(url_for('companies.users', company_id=company_id))
    elif form.is_submitted():
        flash(gettext('Please type in valid information'), 'alert alert-success')
    else:
        form = AdminUserUpdateForm(obj=user)
        form.role.choices = UserRoleTypes.choices()

    return render_template('company/update_user.html', form=form, company_id=company_id, user_id=user_id)


@bp.route('/<company_id>/users/<user_id>/delete', methods=['GET'])
@login_required
@is_admin
def delete_user(company_id, user_id):
    user = AuthUser.get(user_id)

    if not user:
        return redirect('404')

    user.delete()
    return redirect(url_for('companies.users', company_id=company_id))
