from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UnicodeSetAttribute, UTCDateTimeAttribute, BooleanAttribute,
    JSONAttribute)
from werkzeug.security import generate_password_hash, check_password_hash

from backend.models import BaseModel


class Company(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'company'

    id = NumberAttribute(hash_key=True)
    name = UnicodeAttribute()
    address = UnicodeAttribute()
    city = UnicodeAttribute()
    zipcode = UnicodeAttribute()
    country = UnicodeAttribute()
    sector_of_activity = UnicodeAttribute()
    language = UnicodeAttribute(null=True)
