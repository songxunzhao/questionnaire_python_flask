from flask import app
from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, \
    BooleanField, SelectField
from wtforms.validators import DataRequired
from wtforms import Label


class CompanyForm(FlaskForm):
    name = StringField(lazy_gettext('Name'),
                       validators=[DataRequired(message=lazy_gettext("Provide a company name."))])
    address = StringField(lazy_gettext('Address'),
                          validators=[DataRequired(message=lazy_gettext('Provide a company address'))])
    city = StringField(lazy_gettext('City'),
                       validators=[DataRequired(message=lazy_gettext('Provide a company city'))])
    zipcode = StringField(lazy_gettext('Zip code'),
                          validators=[DataRequired(message=lazy_gettext('Provide a zip code'))])
    country = StringField(lazy_gettext('Country'),
                          validators=[DataRequired(message=lazy_gettext('Provide a country name'))])
    sector_of_activity = StringField(lazy_gettext('Sector of Activity'),
                                     validators=[DataRequired(message=lazy_gettext('Provide a sector of activity'))])
    language = SelectField(lazy_gettext('Language'),
                           choices=[])
