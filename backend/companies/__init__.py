from .views import bp


def init_app(app, url_prefix='/companies', api_prefix='/api/companies'):
    app.register_blueprint(bp, url_prefix='/<lang_code>' + url_prefix)
    # app.register_blueprint(bp, url_prefix=url_prefix)
