from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, abort
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )
from backend.companies.models import Company
from backend.contractors.models import Contractor
from backend.decorators import is_company_user

bp = Blueprint('settings', __name__)


@bp.route('/language')
@login_required
@is_company_user
def language():
    contractors = Contractor.scan()
    contractor_list = []

    for contractor in contractors:
        if contractor.company_id:
            company = Company.get(contractor.company_id)
            contractor.company = company

        contractor_list.append(contractor)
    return render_template('setting/language.html')


@bp.route('/help')
def help():
    return render_template('setting/help.html')
