from functools import wraps
from flask import g, request, redirect, url_for
from flask_login import current_user

from backend.auth.choice import UserRoleTypes


def is_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_admin:
            return redirect(url_for('error_403', next=request.url))
        return f(*args, **kwargs)
    return decorated_function


def is_dpo(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.role != UserRoleTypes.DPO.name:
            return redirect(url_for('error_403', next=request.url))
        return f(*args, **kwargs)
    return decorated_function


def is_company_user(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.company_id is None:
            return redirect(url_for('error_403', next=request.url))
        return f(*args, **kwargs)
    return decorated_function
