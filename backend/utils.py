def without_keys(d, keys):
    return {x: d[x] for x in d if x not in keys}


def prepare_contractor_dropdown(treatment_json):
    # Check if the json only has one page
    pages = treatment_json.get('pages')

    page_list = []
    if pages:
        for page in pages:
            page_list.append(page)
    else:
        page_list.append(treatment_json)

    for obj in page_list:
        # question
        question_list = obj.get('questions')
        if not question_list:
            question_list = obj.get('elements')
        if isinstance(question_list, list):
            for question in question_list:
                if 'type' in question and question['type'] == 'dropdown' and 'loadData' in question and question['loadData'] == '#contractor':
                    question['choicesByUrl'] = {
                        'url': '/api/contractors/'
                    }

    return treatment_json


def collect_questions(treatment_json):
    questions = []
    pages = treatment_json.get('pages')

    page_list = []
    if pages:
        for page in pages:
            page_list.append(page)
    else:
        page_list.append(treatment_json)

    for obj in page_list:
        # question
        question_list = obj.get('questions')
        if not question_list:
            question_list = obj.get('elements')

        if question_list:
            if isinstance(question_list, list):
                questions = questions + question_list
            else:
                questions = questions + list(question_list)

    return questions


def total_questions(treatment_json):
    return len(collect_questions(treatment_json))


def num_answers(answer_data):
    return len(answer_data.keys())
