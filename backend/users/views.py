from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, abort, current_app as app
from flask_babel import gettext
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )
from functools import wraps
import yaml
import json
import os
import csv

from backend.auth.choice import UserRoleTypes, UserTypes
from backend.auth.forms import UserCreateForm, UserUpdateForm, UserAssignRoleForm, SearchUserForm
from backend.auth.models import AuthUser
from backend.companies.models import Company
from backend.decorators import is_dpo
from backend.utils import without_keys
from backend.treatment_services.models import TreatmentService
from datetime import datetime

bp = Blueprint('users', __name__)

user_types = {}
for x in UserTypes:
    user_types[gettext(x.name)] = x.value

user_roles = {}
for x in UserRoleTypes:
    user_roles[gettext(x.name)] = x.value

def has_digits(str):
    return any(char.isdigit() for char in str)

def is_authorized(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not(current_user.is_admin or current_user.is_dpo):
            return redirect(url_for('error_403', next=request.url))
        return f(*args, **kwargs)
    return decorated_function


@bp.route('/', methods=['GET'])
@login_required
def home():
    search_form = SearchUserForm(request.args)

    condition = ~AuthUser.username.is_in(current_user.username)
    if search_form.username.data:
        condition = condition & AuthUser.username.startswith(search_form.username.data)

    if current_user.is_admin:
        objects = AuthUser.scan(condition)
    elif current_user.is_dpo:
        company_id = current_user.company_id
        objects = AuthUser.company_index.query(company_id,
                                               filter_condition=condition)
    else:
        return redirect(url_for('error_403'), 403)

    results = []
    for obj in objects:
        try:
            if obj.company_id:
                obj.company = Company.get(obj.company_id)

            if obj.user_type:
                type_name = user_types[obj.user_type]
                if type_name:
                    obj.user_type_name = gettext(type_name)
                else:
                    obj.user_type_name = ''

            if obj.role:
                role_name = user_roles[obj.role]
                if role_name:
                    obj.role_name = gettext(role_name)
                else:
                    obj.role_name = ''

            results.append(obj)
        except Company.DoesNotExist:
            obj.delete()

    print results
    return render_template('user/index.html', users=results)


@bp.route('/add', methods=['GET', 'POST'])
@login_required
@is_authorized
def add():
    form = UserCreateForm()

    if current_user.is_admin:
        # Get all companies
        result = Company.scan()
        companies = []
        for obj in result:
            companies.append((obj.id, obj.name, ))
        form.company_id.choices = companies
    else:
        # Delete company id field
        del form.company_id

    available_roles = tuple((x.name, gettext(x.value)) for x in UserRoleTypes)
    form.role.choices = available_roles

    # available_types = tuple((x.name, gettext(x.value)) for x in UserTypes)
    # form.user_type.choices = available_types
    available_types = []
    last_obj = None
    for x in UserTypes:
        if x.name is not UserTypes.OTHERS.name:
            available_types.append((x.name, gettext(x.value)))
        else:
            last_obj = x
    if last_obj:
        available_types.append((last_obj.name, gettext(last_obj.value)))
    form.user_type.choices = available_types

    service_choices = []
    services = TreatmentService.scan()
    for service in services:
        service_choices.append((service.name, gettext(service.name)))
    form.services_included.choices = service_choices

    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])

        expiration_time = data.get('expiration_date')
        if datetime.now() > expiration_time:
            flash(gettext(u'Please type in valid expiration time'), 'alert alert-success')
            return render_template('user/add.html', form=form)

        if current_user.is_dpo:
            data.update({'company_id': current_user.company_id})

        password = data.pop('password')
        if not has_digits(password) or len(password) < 10:
            flash(gettext("Password has minimum 10 characters and a digit"), "alert alert-success")
            return render_template('user/add.html', form=form)

        user = AuthUser(**data)
        user.password = password
        user.save()

        flash(gettext("User was created successfully"), "alert alert-success")
        return redirect(url_for('users.home'))
    elif form.is_submitted():
        flash(form.errors, "alert alert-success")

    return render_template('user/add.html', form=form)


@bp.route('/<user_id>/update', methods=['GET', 'POST'])
@login_required
@is_authorized
def update(user_id):
    def prepare_choices(form):
        available_roles = tuple((x.name, gettext(x.value)) for x in UserRoleTypes)
        form.role.choices = available_roles
        # available_types = tuple((x.name, gettext(x.value)) for x in UserTypes)
        # form.user_type.choices = available_types
        available_types = []
        last_obj = None
        for x in UserTypes:
            if x.name is not UserTypes.OTHERS.name:
                available_types.append((x.name, gettext(x.value)))
            else:
                last_obj = x
        if last_obj:
            available_types.append((last_obj.name, gettext(last_obj.value)))
        form.user_type.choices = available_types

        if current_user.is_admin:
            result = Company.scan()
            companies = []
            for obj in result:
                companies.append((obj.id, obj.name,))
            form.company_id.choices = companies
        elif current_user.is_dpo:
            del form.company_id

    form = UserUpdateForm()

    if user_id == current_user.username:
        return redirect(url_for('error_403'))

    user = AuthUser.get(user_id)

    if not user:
        redirect('404')
        return

    prepare_choices(form)

    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])
        for key, value in data.items():
            setattr(user, key, value)
        user.save()

        flash(gettext("User was updated successfully"), "alert alert-success")
        return redirect(url_for('users.home'))
    elif form.is_submitted():
        flash(form.errors, 'alert alert-success')
    else:
        form = UserUpdateForm(obj=user)
        prepare_choices(form)

    return render_template('user/update.html', form=form, user_id=user_id)


@bp.route('/assign_role', methods=['GET', 'POST'])
@login_required
@is_authorized
def assign_role():

    form = UserAssignRoleForm()
    available_roles = tuple((x.name, gettext(x.value)) for x in UserRoleTypes)
    form.role.choices = available_roles

    if current_user.is_admin:
        users = AuthUser.scan()
    else:
        users = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in users:
        if current_user.username != obj.username:
            user_choices.append((obj.username, obj.username))

    form.username.choices = user_choices

    if form.validate_on_submit():
        data = without_keys(form.data, ['csrf_token'])
        username = data.pop('username')
        try:
            user = AuthUser.get(username)
        except:
            return abort(404)

        for key, value in data.items():
            setattr(user, key, value)
        user.save()

        flash(gettext("User was updated successfully"), "alert alert-success")
        return redirect(url_for('users.home'))
    elif form.is_submitted():
        flash(gettext('Please type in valid information'), 'alert alert-success')

    return render_template('user/assign_role.html', form=form)


@bp.route('/<user_id>/delete')
@login_required
@is_authorized
def delete(user_id):
    print 'delete called'
    user = AuthUser.get(user_id)

    if not user:
        return render_template('404.html'), 404

    user.delete()

    flash(gettext('Successfully deleted user.'), 'alert alert-success')

    return redirect(url_for('users.home'))

@bp.route('/<user_id>/export', methods=['GET'])
@login_required
def export(user_id):
    user = AuthUser.get(user_id)
    if not user:
        return abort(404)

    try:
        export_path = app.config['EXPORT_PATH']
        filename = 'user_' + user_id + '.csv'
        filepath = os.path.join(export_path, filename)

        company_name = Company.get(user.company_id).name

        with open(filepath, 'wb') as fobj:
            tr_writer = csv.writer(fobj, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            tr_writer.writerow(['UserName', 'FirstName', 'LastName', 'Phone', 'Email', 'Expiration Date',
                                'Role', 'Company'])
            tr_writer.writerow([user.username, user.first_name, user.last_name, user.phone, user.email,
                                user.expiration_date, user.role, company_name])

        with open(filepath, "r") as fobj:
            return Response(
                fobj.read(),
                mimetype="text/csv",
                headers={"Content-disposition": "attachment; filename={}".format(filename)})
    except:
        return render_template('404.html')