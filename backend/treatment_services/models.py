from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UTCDateTimeAttribute, BooleanAttribute,
    JSONAttribute)

from backend.assignments.choice import TreatmentStatusTypes
from backend.models import BaseModel


class TreatmentService(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'treatment_service'

    name = UnicodeAttribute(hash_key=True)
    description = UnicodeAttribute(null=True, default='')

