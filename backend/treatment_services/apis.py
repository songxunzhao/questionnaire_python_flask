from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, jsonify, abort
from flask_login import (UserMixin,
                         login_required,
                         )

from backend.treatment_services.models import TreatmentService

bp = Blueprint('api.treatment_services', __name__)


@bp.route('/all', methods=['GET'])
@login_required
def get_all():

    services = TreatmentService.scan()
    services_list = []
    for service in services:
        services_list.append(service.name)
    return jsonify(services_list)


@bp.route('/add', methods=['POST'])
@login_required
def add():
    service_name = request.form.get('name')
    if not service_name:
        return jsonify({'error': 'Bad Request'}), 404

    service = TreatmentService()
    service.name = service_name
    service.description = request.args.get('description', '')
    service.save()

    return jsonify(service.to_dict())
