from flask import app
from pynamodb.attributes import UnicodeAttribute, NumberAttribute
from pynamodb.models import Model
import os


class BaseModel(Model):
    class Meta:
        region = 'eu-west-3'

    def to_dict(self):
        rval = {}
        for key in self.attribute_values:
            rval[key] = self.__getattribute__(key)
        return rval


class IDGenerator(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'id_generator'

    table_name = UnicodeAttribute(hash_key=True)
    id = NumberAttribute(default=1)

    @staticmethod
    def generate_id(table_name):
        try:
            obj = IDGenerator.get(table_name)
        except:
            obj = IDGenerator(table_name=table_name, id=1)

        id_num = obj.id
        obj.id += 1
        obj.save()
        return id_num
