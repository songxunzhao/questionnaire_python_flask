from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, jsonify, abort
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )

from backend.activity_sectors.models import ActivitySector


bp = Blueprint('api.activity_sectors', __name__)


@bp.route('/all', methods=['GET'])
@login_required
def get_all():
    sectors = ActivitySector.scan()
    sector_list = []
    for sector in sectors:
        sector_list.append(sector.name)
    return jsonify(sector_list)


@bp.route('/add', methods=['POST'])
@login_required
def add():
    if not current_user.is_dpo:
        return jsonify({'error': 'Not Authorized Access'}, 403)

    sector_name = request.form.get('name')
    if not sector_name:
        return jsonify({'error': 'Bad Request'}), 404

    sector = ActivitySector()
    sector.name = sector_name
    sector.description = request.args.get('description', '')
    sector.save()

    return jsonify(sector.to_dict())
