from .apis import bp as api_bp


def init_app(app, api_prefix='/api/activity_sectors'):
    app.register_blueprint(api_bp, url_prefix=api_prefix)
