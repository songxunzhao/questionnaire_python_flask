from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UTCDateTimeAttribute, BooleanAttribute,
    JSONAttribute)

from backend.models import BaseModel


class ActivitySector(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'activity_sector'

    name = UnicodeAttribute(hash_key=True)
    description = UnicodeAttribute(null=True, default='')
