from flask_babel import gettext, lazy_gettext

from backend.enums import ChoiceEnum


class TreatmentStatusTypes(ChoiceEnum):
    OPEN = lazy_gettext('Open')
    IN_PROGRESS = lazy_gettext('In Progress')
    COMPLETED = lazy_gettext('Completed')
    VALIDATED = lazy_gettext('Validated')


class TreatmentDataSensibilityTypes(ChoiceEnum):
    NONE = lazy_gettext('None')
    GENETIC_DATA = lazy_gettext('Genetic data')
    HEALTH_DATA = lazy_gettext('Health data')
    SEX_DATA = lazy_gettext('Contain sex data')
    CHILDREN_DATA = lazy_gettext('Children data')
