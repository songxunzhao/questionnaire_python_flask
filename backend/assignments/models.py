from pynamodb.indexes import (
    GlobalSecondaryIndex, 
    AllProjection, 
    KeysOnlyProjection, 
    IncludeProjection
)
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, NumberAttribute, UTCDateTimeAttribute, BooleanAttribute,
    JSONAttribute)

from backend.assignments.choice import TreatmentStatusTypes
from backend.auth.choice import TreatmentTypes
from backend.models import BaseModel


class TreatmentRegisterIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'treatment-register-index'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    register_id = NumberAttribute(hash_key=True)
    created_at = UTCDateTimeAttribute(range_key=True)


class TreatmentCompanyIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'treatment-company-index'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    company_id = NumberAttribute(hash_key=True)
    created_at = UTCDateTimeAttribute(range_key=True)


class Treatment(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'treatment'


    id = UnicodeAttribute(hash_key=True)
    name = UnicodeAttribute()
    created_at = UTCDateTimeAttribute()
    status = UnicodeAttribute(default=TreatmentStatusTypes.OPEN.name)
    created_by = UnicodeAttribute()
    assigned_to = UnicodeAttribute(null=True)
    validated_by = UnicodeAttribute(null=True)
    validation_status = UnicodeAttribute(null=True)
    type = UnicodeAttribute(default=TreatmentTypes.NONE_AUDIT.name)
    service_concerned = UnicodeAttribute(null=True)
    need_PIA = BooleanAttribute(default=False)
    data_sensibility = UnicodeAttribute(null=True)
    risk_exist = BooleanAttribute(default=False)
    progression_status = NumberAttribute(default=0)
    audit_end_date = UTCDateTimeAttribute()
    register_id = NumberAttribute(null=True)
    company_id = NumberAttribute()
    detail = JSONAttribute()  # Survey questionnaire/treatment data
    audit_result = JSONAttribute(null=True)  # Survey Answer data

    register_index = TreatmentRegisterIndex()
    company_index = TreatmentCompanyIndex()

    @property
    def is_validated(self):
        return self.status == TreatmentStatusTypes.VALIDATED.name


class RegisterCompanyIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'register-company-index'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    company_id = NumberAttribute(hash_key=True)
    created_at = UTCDateTimeAttribute(range_key=True)


class Register(BaseModel):
    class Meta(BaseModel.Meta):
        table_name = 'register'

    id = NumberAttribute(hash_key=True)
    name = UnicodeAttribute()
    assigned_to = UnicodeAttribute(null=True)
    company_id = NumberAttribute()
    created_by = UnicodeAttribute()
    created_at = UTCDateTimeAttribute()
    company_index = RegisterCompanyIndex()
    from_template = BooleanAttribute()
    template = UnicodeAttribute(null=True)

