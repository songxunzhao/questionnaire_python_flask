import datetime

import dateutil
from flask import render_template, flash, url_for, \
    redirect, request, Response, Blueprint, jsonify, abort
from flask_login import (UserMixin,
                         login_required,
                         login_user,
                         logout_user,
                         current_user
                         )

from backend.assignments.choice import TreatmentStatusTypes
from backend.assignments.models import Treatment
from backend.decorators import is_company_user, is_dpo
from backend.utils import total_questions, num_answers

bp = Blueprint('api.assignments', __name__)


@bp.route('/treatments/<treatment_id>/save_audit', methods=['POST'])
@login_required
@is_company_user
def save_audit(treatment_id):
    company_id = current_user.company_id

    try:
        treatment = Treatment.get(treatment_id)
    except:
        return abort(404)

    if treatment.status == TreatmentStatusTypes.VALIDATED.name:
        return abort(404)

    data = request.get_json()
    treatment.audit_result = data

    treatment.progression_status = float(num_answers(data)) / total_questions(treatment.detail) * 100

    if treatment.progression_status == 100:
        treatment.status = TreatmentStatusTypes.COMPLETED.name
    elif treatment.progression_status == 0:
        treatment.status = TreatmentStatusTypes.OPEN.name
    else:
        treatment.status = TreatmentStatusTypes.IN_PROGRESS.name

    treatment.save()

    return jsonify(treatment.to_dict())


@bp.route('/treatment/<treatment_id>/status', methods=['POST'])
@login_required
@is_dpo
def treatment_status(treatment_id):
    try:
        treatment = Treatment.get(treatment_id)
        treatment.status = request.json.get('status')
        treatment.save()
        return jsonify(treatment.to_dict())
    except Treatment.DoesNotExist:
        return abort(404)
