import datetime
import json
import os
import uuid
from os.path import isfile
import unicodecsv as csv
import dateutil.parser
import yaml
import io
import pdfkit

from flask import (
    render_template,
    flash,
    url_for,
    redirect,
    request,
    Blueprint,
    jsonify,
    current_app as app,
    abort, g, Response, make_response)
from flask_babel import gettext
from flask_login import (login_required,
                         current_user
                         )

from backend.assignments.choice import TreatmentStatusTypes
from backend.assignments.forms import RegisterAssignForm, RegisterManualAssignForm, TreatmentUpdateForm, \
    SearchTreatmentForm, TreatmentCreateForm
from backend.assignments.models import Register, Treatment
from backend.auth.choice import TreatmentTypes
from backend.auth.models import AuthUser
from backend.models import IDGenerator
from backend.treatment_services.models import TreatmentService
from backend.tasks.choice import CategoryChoice, priority_choice
from backend.tasks.forms import TaskForm
from backend.utils import without_keys, prepare_contractor_dropdown, collect_questions
from backend.data_sensibility.models import DataSensibility

bp = Blueprint('assignments', __name__)


def get_register_path():
    return os.path.join(app.config['REGISTER_PATH'], g.lang_code)


def get_visible_registers():
    if current_user.is_admin:
        registers = Register.scan()
    elif current_user.is_dpo and current_user.is_data_manager:
        registers = Register.company_index.query(current_user.company_id)
    else:
        registers = Register.company_index.query(current_user.company_id,
                                                 filter_condition=Register.assigned_to == current_user.username)
    return registers


@bp.route('')
@login_required
def home():
    registers = get_visible_registers()

    return render_template('assignment/index.html', registers=registers)


@bp.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    filename = request.args.get('filename')

    register_path = get_register_path()
    register_files = [(f, f) for f in os.listdir(register_path) if isfile(os.path.join(register_path, f))]

    if filename and (filename, filename) not in register_files:
        redirect(url_for('error_404'))

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))

    form = RegisterManualAssignForm()
    form.assigned_to.choices = user_choices
    form.assigned_to.data = current_user.username
    form.name.choices = register_files
    form.template.choices = register_files

    if form.validate_on_submit():
        form_data = without_keys(form.data, ['csrf_token'])
        register = Register(**form_data)
        register.id = IDGenerator.generate_id(Register.Meta.table_name)
        register.company_id = current_user.company_id
        register.created_by = current_user.username
        register.created_at = datetime.datetime.now()
        register.save()

        if form.from_template.data:
            filepath = os.path.join(register_path, form_data['template'])

            with io.open(filepath, 'r', encoding='utf8') as fobj:
                register_data = yaml.load(fobj)

                treatment_data = register_data.pop('treatments')
                for treatment in treatment_data:
                    obj = Treatment()
                    obj.id = str(uuid.uuid4())
                    obj.name = treatment.get('name')
                    obj.register_id = register.id
                    obj.assigned_to = form_data['assigned_to']
                    obj.created_at = datetime.datetime.now()
                    obj.created_by = current_user.username
                    obj.company_id = current_user.company_id
                    obj.audit_end_date = datetime.datetime.now() + datetime.timedelta(days=10)
                    obj.detail = treatment
                    obj.save()

        return redirect(url_for('assignments.home'))
    elif form.is_submitted():
        flash(gettext('Please type in valid information.'))
    else:
        if filename:
            form.template.data = filename
            form.from_template.data = True
    return render_template('assignment/create.html', form=form)


@bp.route('/<register_id>/delete')
@login_required
def delete_register(register_id):
    register_id = int(register_id)
    register = Register.get(register_id)
    register.delete()

    objects = Treatment.register_index.query(register_id)
    for obj in objects:
        obj.delete()

    flash(gettext('Successfully deleted register.'), 'alert alert-success')

    return redirect(url_for('assignments.home'))


@bp.route('/<register_id>/update', methods=['GET', 'POST'])
@login_required
def update(register_id):
    register_id = int(register_id)
    register = Register.get(register_id)

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))

    form = RegisterAssignForm()

    if form.validate_on_submit():
        for key, value in form.data.items():
            setattr(register, key, value)
        register.save()

        # objects = Treatment.register_index.query(register_id)
        #
        # for obj in objects:
        #     obj.assigned_to = form.data['assigned_to']
        #     obj.save()

        return redirect(url_for('assignments.home'))
    elif form.is_submitted():
        flash(gettext(u'Please type in valid information'), 'alert alert-success')
    else:
        form = RegisterAssignForm(obj=register)

        return render_template('assignment/update.html', form=form, register_id=register_id)


@bp.route('/treatments/create', methods=['GET', 'POST'])
@login_required
def create_treatment():
    def set_choices(_form):
        _form.template.choices = template_choices
        _form.validated_by.choices = validated_user_choices
        _form.type.choices = type_choices
        _form.register_id.choices = register_choices
        _form.service_concerned.choices = service_choices
        _form.assigned_to.choices = user_choices
        _form.assigned_to.data = current_user.username
        _form.data_sensibility.choices = data_sensibility_choices

    pia = request.args.get('pia', False)
    pia_prefix = 'pia_'
    register_path = get_register_path()
    template_choices = []

    # Collect all treatments
    for f in os.listdir(register_path):
        if pia and not f.startswith(pia_prefix):
            continue
        elif not pia and f.startswith(pia_prefix):
            continue

        filepath = os.path.join(register_path, f)
        filename = None
        if isfile(filepath):
            with io.open(filepath, 'r', encoding='utf-8') as fobj:
                try:
                    data = yaml.load(fobj)
                    for index in range(len(data['treatments'])):
                        tr = data['treatments'][index]
                        template_choices.append((f + '/' + str(index), f + '/' + tr['name'],))
                except yaml.YAMLError as exc:
                    print exc

    if current_user.is_admin:
        return abort(403)
    else:
        objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    validated_user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))
        if obj.is_dpo or obj.is_data_manager:
            validated_user_choices.append((obj.username, obj.username))

    treatment_types = TreatmentTypes.choices()
    type_choices = []
    last_obj = None
    for obj in treatment_types:
        if obj[0] == 'NONE_AUDIT':
            last_obj = obj
        else:
            type_choices.append(obj)
    type_choices.append(last_obj)

    register_choices = []
    # objects = Register.scan()
    objects = get_visible_registers()
    for obj in objects:
        register_choices.append((obj.id, obj.name,))

    service_choices = []
    services = TreatmentService.scan()
    for service in services:
        service_choices.append((service.name, service.name))

    data_sensibility_choices = []
    objects = DataSensibility.scan()
    last_obj = None
    for obj in objects:
        if obj.name == 'Not applicable':
            last_obj = obj
        else:
            data_sensibility_choices.append((obj.name, gettext(obj.name)))
    if last_obj:
        data_sensibility_choices.append((last_obj.name, gettext(last_obj.name)))

    form = TreatmentCreateForm()
    set_choices(form)

    if form.validate_on_submit():
        template = form.data.pop('template')
        treatment = Treatment(**without_keys(form.data, ['csrf_token', 'template']))
        treatment.id = str(uuid.uuid4())
        treatment.created_at = datetime.datetime.now()
        treatment.created_by = current_user.username
        treatment.company_id = current_user.company_id
        treatment.status = TreatmentStatusTypes.OPEN.name
        treatment.need_PIA = pia

        tpath = template.split('/')
        # Open template path
        filepath = os.path.join(register_path, tpath[0])

        with io.open(filepath, 'r', encoding='utf-8') as fobj:
            try:
                data = yaml.load(fobj)
                tr = data['treatments'][int(tpath[1])]
                treatment.detail = tr['content']
            except:
                flash(gettext(u'Please type in valid information'), 'alert alert-success')

        treatment.save()
        return redirect(url_for('assignments.treatments'))
    elif form.is_submitted():
        flash(gettext(json.dumps(form.errors)), 'alert alert-success')

    # prepare additional arguments

    return render_template('assignment/create_treatment.html', form=form)


@bp.route('/treatments/<treatment_id>/update', methods=['GET', 'POST'])
@login_required
def update_treatment(treatment_id):
    def set_choices(_form):
        _form.assigned_to.choices = user_choices
        _form.status.choices = TreatmentStatusTypes.choices()
        _form.validated_by.choices = validated_user_choices
        _form.type.choices = type_choices
        _form.service_concerned.choices = service_choices
        _form.data_sensibility.choices = data_sensibility_choices

    register_id = request.args.get('register_id')

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    validated_user_choices = []
    for obj in objects:
        user_choices.append((obj.username, gettext(obj.username)))
        if obj.is_dpo or obj.is_data_manager:
            validated_user_choices.append((obj.username, obj.username))

    services = TreatmentService.scan()

    # service_choices = [(0, '')]
    service_choices = []
    for service in services:
        service_choices.append((service.name, service.name))

    treatment_types = TreatmentTypes.choices()
    type_choices = []
    last_obj = None
    for obj in treatment_types:
        if obj[0] == 'NONE_AUDIT':
            last_obj = obj
        else:
            type_choices.append(obj)
    type_choices.append(last_obj)

    treatment = Treatment.get(treatment_id)

    data_sensibility_choices = []
    objects = DataSensibility.scan()
    last_obj = None
    for obj in objects:
        if obj.name == 'Not applicable':
            last_obj = obj
        else:
            data_sensibility_choices.append((obj.name, gettext(obj.name)))
    if last_obj:
        data_sensibility_choices.append((last_obj.name, gettext(last_obj.name)))

    form = TreatmentUpdateForm()
    set_choices(form)

    if form.validate_on_submit():
        for key, value in form.data.items():
            setattr(treatment, key, value)
        treatment.save()
        return redirect(url_for('assignments.treatments'))
    elif form.is_submitted():
        flash(gettext(u'Please type in valid information'), 'alert alert-success')
    else:
        form = TreatmentUpdateForm(obj=treatment)
        set_choices(form)

    # prepare additional arguments
    kwargs = {
        'form': form,
        'treatment': treatment
    }

    if register_id:
        register_id = int(register_id)
        kwargs.update({
            'register_id': register_id
        })

    return render_template('assignment/update_treatment.html',
                           **kwargs)


@bp.route('/treatments/<treatment_id>/delete')
@login_required
def delete_treatment(treatment_id):
    register_id = request.args.get('register_id')
    treatment = Treatment.get(treatment_id)
    treatment.delete()

    return redirect(url_for('assignments.treatments'))


@bp.route('/treatments/<treatment_id>/play')
@login_required
def play_treatment(treatment_id):
    treatment = Treatment.get(treatment_id)
    detail = prepare_contractor_dropdown(treatment.detail)

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))

    return render_template('assignment/play.html',
                           treatment_json=json.dumps(detail),
                           treatment_answer=json.dumps(treatment.audit_result),
                           user_choices=user_choices,
                           treatment_id=treatment_id)


@bp.route('/treatments/<treatment_id>/view')
@login_required
def view_treatment(treatment_id):
    treatment = Treatment.get(treatment_id)
    detail = prepare_contractor_dropdown(treatment.detail)

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))

    return render_template('assignment/play.html',
                           treatment_json=json.dumps(detail),
                           treatment_answer=json.dumps(treatment.audit_result),
                           treatment_id=treatment_id,
                           user_choices=user_choices,
                           readonly=True)


@bp.route('/treatments')
@login_required
def treatments():
    register_id = request.args.get('register_id')

    search_form = SearchTreatmentForm(request.args)
    status_options = TreatmentStatusTypes.choices()

    registers = get_visible_registers()

    created_at = request.args.get('before')

    if created_at is None:
        created_at = datetime.datetime.now()
    else:
        created_at = dateutil.parser.parse(created_at)

    register_options = []
    for obj in registers:
        register_options.append((obj.id, obj.name,))

    search_form.status.choices = (('', ''),) + status_options

    search_form.register_id.choices = [('', '')] + register_options

    total_count = Treatment.count()

    return render_template('assignment/treatments.html',
                           total_count=total_count,
                           search_form=search_form
                           )


@bp.route('/treatment_list', methods=['POST'])
@login_required
def treatment_list():
    # created_at = request.args.get('before')

    # if created_at is None:
    #     created_at = datetime.datetime.now()
    # else:
    #     created_at = dateutil.parser.parse(created_at)

    last_evaluated_key = request.json.get('last_evaluated_key')

    search_name = request.json.get('name')

    search_status = request.json.get('status')

    search_register_id = request.json.get('register_id')

    filter_condition = None

    if search_name:
        condition = Treatment.name.startswith(search_name)
        if filter_condition is not None:
            filter_condition = filter_condition & condition
        else:
            filter_condition = condition

    if search_status:
        condition = Treatment.status == search_status
        if filter_condition is not None:
            filter_condition = filter_condition & condition
        else:
            filter_condition = condition

    if search_register_id:
        condition = Treatment.register_id == int(search_register_id)
        if filter_condition is not None:
            filter_condition = filter_condition & condition
        else:
            filter_condition = condition

    print filter_condition

    if current_user.is_admin:
        results = Treatment.scan(
            filter_condition,
            limit=5,
            last_evaluated_key=last_evaluated_key
        )

    else:
        results = Treatment.company_index.query(
            current_user.company_id,
            filter_condition,
            scan_index_forward=False,
            last_evaluated_key=last_evaluated_key,
            limit=5)

    # treatment_objects = list(results)
    treatment_objects = []
    typeChoices = dict(TreatmentTypes.choices())
    statusChoices = dict(TreatmentStatusTypes.choices())
    for obj in results:
        if obj.register_id:
            register = Register.get(obj.register_id)
            obj.register_name = register.name

        if obj.type:
            obj.type_name = typeChoices[obj.type]

        if obj.status:
            obj.status_name = statusChoices[obj.status]

        treatment_objects.append(obj)

    last_evaluated_key = results.last_evaluated_key

    print treatment_objects

    return render_template('assignment/treatment_list.html',
                           last_evaluated_key=json.dumps(last_evaluated_key),
                           items=treatment_objects,
                           treatment_status_types=TreatmentStatusTypes.choices()
                           )


@bp.route('/treatments/<treatment_id>/export', methods=['GET'])
@login_required
def export_treatment(treatment_id):
    try:
        export_path = app.config['EXPORT_PATH']
        filename = 'treatment_' + treatment_id + '.csv'
        filepath = os.path.join(export_path, filename)
        treatment = Treatment.get(treatment_id)
        with open(filepath, 'wb') as fobj:
            tr_writer = csv.writer(fobj, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            detail = treatment.detail
            audit_result = treatment.audit_result if treatment.audit_result else {}
            questions = collect_questions(detail)
            # print audit_result
            tr_writer.writerow(['Name', 'Question', 'Answer'])
            for question in questions:
                audit_result.get(question.get('name', ''), '')
                tr_writer.writerow(
                    [question.get('name'), question.get('title'), audit_result.get(question.get('name'), '')])

        with open(filepath, "r") as fobj:
            return Response(
                fobj.read(),
                mimetype="text/csv",
                headers={"Content-disposition": "attachment; filename={}".format(filename)})

    except Treatment.DoesNotExist:
        return abort(404)


@bp.route('/treatments/<treatment_id>/export_pdf', methods=['GET'])
@login_required
def export_treatment_to_pdf(treatment_id):
    treatment = Treatment.get(treatment_id)
    detail = prepare_contractor_dropdown(treatment.detail)

    objects = AuthUser.company_index.query(current_user.company_id)

    user_choices = []
    for obj in objects:
        user_choices.append((obj.username, obj.username))

    return render_template('assignment/treatment_export_template.html',
                           treatment_json=json.dumps(detail),
                           treatment_answer=json.dumps(treatment.audit_result),
                           user_choices=user_choices,
                           treatment_id=treatment_id)


@bp.route('/treatments/export_all', methods=['GET'])
@login_required
def export_all_treatment():
    try:
        export_path = app.config['EXPORT_PATH']
        filename = 'treatments' + '.csv'
        filepath = os.path.join(export_path, filename)

        if current_user.is_admin:
            treatments = Treatment.scan()

        else:
            treatments = Treatment.company_index.query(
                current_user.company_id
            )

        with open(filepath, 'wb') as fobj:
            tr_writer = csv.writer(fobj, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            tr_writer.writerow(['name', 'created at', 'status', 'created by', 'assigned to', 'validated by',
                                'validation status', 'type', 'service concerned', 'need PIA', 'data sensibility',
                                'risk exists', 'progression status', 'audit end date', 'register id', 'company id'])

            for treatment in treatments:
                print(treatment)
                tr_writer.writerow([treatment.name, treatment.created_at, treatment.status, treatment.created_by,
                                    treatment.assigned_to,
                                    treatment.validated_by, treatment.validation_status, treatment.type,
                                    treatment.service_concerned,
                                    treatment.need_PIA, treatment.data_sensibility, treatment.risk_exist,
                                    treatment.progression_status,
                                    treatment.audit_end_date, treatment.register_id, treatment.company_id])

        with open(filepath, "r") as fobj:
            return Response(
                fobj.read(),
                mimetype="text/csv",
                headers={"Content-disposition": "attachment; filename={}".format(filename)})

    except Treatment.DoesNotExist:
        return abort(404)


@bp.route('/treatments/<treatment_id>/duplicate', methods=['GET', 'POST'])
@login_required
def duplicate_treatment(treatment_id):
    try:
        treatment = Treatment.get(treatment_id)

        duplicatedObj = Treatment()

        duplicatedObj.id = str(uuid.uuid4())
        duplicatedObj.name = 'copy-' + treatment.name
        duplicatedObj.created_at = treatment.created_at
        duplicatedObj.status = treatment.status
        duplicatedObj.created_by = treatment.created_by
        duplicatedObj.assigned_to = treatment.assigned_to
        duplicatedObj.validated_by = treatment.validated_by
        duplicatedObj.validation_status = treatment.validation_status
        duplicatedObj.type = treatment.type
        duplicatedObj.service_concerned = treatment.service_concerned
        duplicatedObj.need_PIA = treatment.need_PIA
        duplicatedObj.data_sensibility = treatment.data_sensibility
        duplicatedObj.risk_exist = treatment.risk_exist
        duplicatedObj.progression_status = treatment.progression_status
        duplicatedObj.audit_end_date = treatment.audit_end_date
        duplicatedObj.register_id = treatment.register_id
        duplicatedObj.company_id = treatment.company_id
        duplicatedObj.detail = treatment.detail
        duplicatedObj.audit_result = treatment.audit_result

        duplicatedObj.save()

        search_form = SearchTreatmentForm(request.args)
        status_options = TreatmentStatusTypes.choices()

        registers = get_visible_registers()
        register_options = []
        for obj in registers:
            register_options.append((obj.id, obj.name,))

        search_form.status.choices = (('', ''),) + status_options
        search_form.register_id.choices = [('', '')] + register_options

        return render_template('assignment/treatments.html',
                               total_count=Treatment.count(),
                               search_form=search_form
                               )
    except Treatment.DoesNotExist:
        return abort(404)
