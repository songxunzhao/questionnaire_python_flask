from flask_babel import gettext, lazy_gettext
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, BooleanField, DateTimeField, validators
from wtforms.validators import Optional, DataRequired


class RegisterAssignForm(FlaskForm):
    name = StringField(lazy_gettext('Register'))


class RegisterManualAssignForm(FlaskForm):
    name = StringField(lazy_gettext('Name'))
    assigned_to = SelectField(lazy_gettext('Assign To'))
    template = SelectField(lazy_gettext('Template'))
    from_template = BooleanField(lazy_gettext('From template'))


class TreatmentUpdateForm(FlaskForm):
    name = StringField(lazy_gettext('Name'), validators=[DataRequired()])
    assigned_to = SelectField(lazy_gettext('Assign To'))
    status = SelectField(lazy_gettext('Status'))
    validated_by = SelectField(lazy_gettext('Validated by'))
    type = SelectField(lazy_gettext('Type of data'))
    service_concerned = SelectField(lazy_gettext('Service concerned'))  # coerce=int
    need_PIA = BooleanField(lazy_gettext('Need PIA?'))
    data_sensibility = SelectField(lazy_gettext('Sensibility of data'))
    risk_exist = BooleanField(lazy_gettext('Risk exist?'))
    audit_end_date = DateTimeField(lazy_gettext('End of audit'), format='%m/%d/%Y')


class TreatmentCreateForm(FlaskForm):
    name = StringField(lazy_gettext('Name'), validators=[DataRequired()])
    register_id = SelectField(lazy_gettext('Register'), coerce=int, validators=[Optional()])
    assigned_to = SelectField(lazy_gettext('Assign To'), validators=[Optional()])
    validated_by = SelectField(lazy_gettext('Validated by'))
    type = SelectField(lazy_gettext('Type of data'))
    service_concerned = SelectField(lazy_gettext('Service concerned'))
    need_PIA = BooleanField(lazy_gettext('Need PIA?'))
    data_sensibility = SelectField(lazy_gettext('Sensibility of data'))
    risk_exist = BooleanField(lazy_gettext('Risk exist?'))
    audit_end_date = DateTimeField(lazy_gettext('End of audit'), format='%m/%d/%Y')
    template = SelectField(lazy_gettext('Template'), validators=[DataRequired()])


class SearchTreatmentForm(FlaskForm):
    name = StringField(lazy_gettext('Name'), default='', validators=[validators.optional()])
    status = SelectField(lazy_gettext('Status'), default='', validators=[validators.optional()])
    register_id = SelectField(lazy_gettext('Register'), default='', validators=[validators.optional()])
