from .views import bp
from .apis import bp as api_bp


def init_app(app, url_prefix='/assignments', api_prefix='/api/assignments'):

    app.register_blueprint(bp, url_prefix='/<lang_code>' + url_prefix)
    # app.register_blueprint(bp, url_prefix=url_prefix)
    app.register_blueprint(api_bp, url_prefix=api_prefix)
