"""Manger script."""
import os
import click
from backend.app import app
from backend.auth.models import AuthUser


@app.shell_context_processor
def make_shell_context():
    """make variables available in shell."""
    return dict(
        app=app,
        AuthUser=AuthUser
    )


@app.cli.command()
def test():
    """Run the unit tests."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


@app.cli.command()
def createuser():
    user = AuthUser(username='superuser')
    user.company_id = 0
    user.password = 'qwert'
    user.is_admin = True
    user.save()


@app.cli.command()
@click.argument('method')
def migrate(method):
    from migrations import migrations
    if method == 'up':
        migrations.up()
    elif method == 'down':
        migrations.down()


@app.cli.command()
def seed():
    import seed
    seed.run()

if __name__ == '__main__':
    app.run()
