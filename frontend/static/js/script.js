$(document).ready(function () {
    // Bootstrap scripts
    $('[datepicker]').datepicker({
        uiLibrary: 'bootstrap4',
        showOnFocus: true,
        showRightIcon: false,
        format: 'mm/dd/yyyy'
    });

    $('[data-control="href"]').click(function() {
        window.location.href = $(this).data('url');
    })

    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });


        return false;
    });
});

